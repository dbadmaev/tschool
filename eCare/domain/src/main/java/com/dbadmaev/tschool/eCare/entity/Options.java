package com.dbadmaev.tschool.eCare.entity;

import com.dbadmaev.tschool.eCare.entity.enums.OptionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "options", schema = "eCare")
public class Options implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private long version;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private OptionType optionType;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "connection_cost")
    private BigDecimal connectionCost;

    @Column(name = "is_active")
    private boolean active;

    @ManyToMany(mappedBy = "optionsSet", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Tariffs> tariffsList;

    @ManyToMany(mappedBy = "contractOptionsSet", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Contracts> contractsList;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "incompatible_options",
            joinColumns = {@JoinColumn(name = "option_one_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "option_two_id", referencedColumnName = "id")})
    private List<Options> incompatibleOptionsList = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "linked_options",
            joinColumns = {@JoinColumn(name = "option_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "linked_option", referencedColumnName = "id")})
    private List<Options> linkedOptionsList = new ArrayList<Options>();
}