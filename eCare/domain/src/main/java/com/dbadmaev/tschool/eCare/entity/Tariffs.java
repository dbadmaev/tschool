package com.dbadmaev.tschool.eCare.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "tariffs", schema = "eCare")
public class Tariffs implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private long version;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "status")
    private int status;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @JoinTable(
            name = "tariffs_options",
            joinColumns = {
                    @JoinColumn(name = "tariff_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "option_id", referencedColumnName = "id")
            }
    )
    private List<Options> optionsSet;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "tariff", cascade = {CascadeType.ALL, CascadeType.REMOVE})
    private List<Contracts> contractsList;

}
