package com.dbadmaev.tschool.eCare.entity.enums;

public enum OptionType {
    INTERNET,
    CALLS,
    SMS
}
