package com.dbadmaev.tschool.eCare.entity.enums;

public enum ContractStatus {
    FREE,
    ACTIVE,
    BLOCKED_BY_USER,
    BLOCKED_BY_ADMIN
}
