package com.dbadmaev.tschool.eCare.entity;

import com.dbadmaev.tschool.eCare.entity.enums.ContractStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "contracts", schema = "eCare")
public class Contracts implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private long version;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ContractStatus status;

    @Column(name = "number")
    private String number;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "client_id")
    private Clients client;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "tariff_id")
    private Tariffs tariff;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @JoinTable(
            name = "contracts_options",
            joinColumns = {
                    @JoinColumn(name = "contract_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "option_id", referencedColumnName = "id")
            }
    )
    private List<Options> contractOptionsSet;
}
