package com.dbadmaev.tschool.eCare.entity;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Admins.class)
public abstract class Admins_ {

	public static volatile SingularAttribute<Admins, String> password;
	public static volatile SingularAttribute<Admins, String> role;
	public static volatile SingularAttribute<Admins, String> name;
	public static volatile SingularAttribute<Admins, Long> id;
	public static volatile SingularAttribute<Admins, Long> version;

	public static final String PASSWORD = "password";
	public static final String ROLE = "role";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String VERSION = "version";

}

