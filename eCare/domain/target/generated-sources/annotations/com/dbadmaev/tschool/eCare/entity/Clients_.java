package com.dbadmaev.tschool.eCare.entity;

import java.time.LocalDate;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Clients.class)
public abstract class Clients_ {

	public static volatile SingularAttribute<Clients, String> passportData;
	public static volatile SingularAttribute<Clients, String> password;
	public static volatile ListAttribute<Clients, Contracts> contractsList;
	public static volatile SingularAttribute<Clients, String> role;
	public static volatile SingularAttribute<Clients, String> address;
	public static volatile SingularAttribute<Clients, Integer> blocked;
	public static volatile SingularAttribute<Clients, String> name;
	public static volatile SingularAttribute<Clients, LocalDate> dateOfBirth;
	public static volatile SingularAttribute<Clients, Long> id;
	public static volatile SingularAttribute<Clients, Long> version;
	public static volatile SingularAttribute<Clients, String> email;
	public static volatile SingularAttribute<Clients, String> secondName;

	public static final String PASSPORT_DATA = "passportData";
	public static final String PASSWORD = "password";
	public static final String CONTRACTS_LIST = "contractsList";
	public static final String ROLE = "role";
	public static final String ADDRESS = "address";
	public static final String BLOCKED = "blocked";
	public static final String NAME = "name";
	public static final String DATE_OF_BIRTH = "dateOfBirth";
	public static final String ID = "id";
	public static final String VERSION = "version";
	public static final String EMAIL = "email";
	public static final String SECOND_NAME = "secondName";

}

