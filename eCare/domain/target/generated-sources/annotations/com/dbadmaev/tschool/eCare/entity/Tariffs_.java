package com.dbadmaev.tschool.eCare.entity;

import java.math.BigDecimal;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tariffs.class)
public abstract class Tariffs_ {

	public static volatile ListAttribute<Tariffs, Contracts> contractsList;
	public static volatile SingularAttribute<Tariffs, BigDecimal> price;
	public static volatile SingularAttribute<Tariffs, String> name;
	public static volatile ListAttribute<Tariffs, Options> optionsSet;
	public static volatile SingularAttribute<Tariffs, Long> id;
	public static volatile SingularAttribute<Tariffs, Long> version;
	public static volatile SingularAttribute<Tariffs, Integer> status;

	public static final String CONTRACTS_LIST = "contractsList";
	public static final String PRICE = "price";
	public static final String NAME = "name";
	public static final String OPTIONS_SET = "optionsSet";
	public static final String ID = "id";
	public static final String VERSION = "version";
	public static final String STATUS = "status";

}

