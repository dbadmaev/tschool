package com.dbadmaev.tschool.eCare.entity;

import com.dbadmaev.tschool.eCare.entity.enums.OptionType;
import java.math.BigDecimal;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Options.class)
public abstract class Options_ {

	public static volatile SingularAttribute<Options, OptionType> optionType;
	public static volatile ListAttribute<Options, Tariffs> tariffsList;
	public static volatile ListAttribute<Options, Options> linkedOptionsList;
	public static volatile ListAttribute<Options, Contracts> contractsList;
	public static volatile SingularAttribute<Options, BigDecimal> connectionCost;
	public static volatile SingularAttribute<Options, BigDecimal> price;
	public static volatile SingularAttribute<Options, String> name;
	public static volatile ListAttribute<Options, Options> incompatibleOptionsList;
	public static volatile SingularAttribute<Options, Boolean> active;
	public static volatile SingularAttribute<Options, Long> id;
	public static volatile SingularAttribute<Options, Long> version;

	public static final String OPTION_TYPE = "optionType";
	public static final String TARIFFS_LIST = "tariffsList";
	public static final String LINKED_OPTIONS_LIST = "linkedOptionsList";
	public static final String CONTRACTS_LIST = "contractsList";
	public static final String CONNECTION_COST = "connectionCost";
	public static final String PRICE = "price";
	public static final String NAME = "name";
	public static final String INCOMPATIBLE_OPTIONS_LIST = "incompatibleOptionsList";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String VERSION = "version";

}

