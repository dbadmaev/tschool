package com.dbadmaev.tschool.eCare.entity;

import com.dbadmaev.tschool.eCare.entity.enums.ContractStatus;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Contracts.class)
public abstract class Contracts_ {

	public static volatile SingularAttribute<Contracts, String> number;
	public static volatile ListAttribute<Contracts, Options> contractOptionsSet;
	public static volatile SingularAttribute<Contracts, Clients> client;
	public static volatile SingularAttribute<Contracts, Tariffs> tariff;
	public static volatile SingularAttribute<Contracts, Long> id;
	public static volatile SingularAttribute<Contracts, Long> version;
	public static volatile SingularAttribute<Contracts, ContractStatus> status;

	public static final String NUMBER = "number";
	public static final String CONTRACT_OPTIONS_SET = "contractOptionsSet";
	public static final String CLIENT = "client";
	public static final String TARIFF = "tariff";
	public static final String ID = "id";
	public static final String VERSION = "version";
	public static final String STATUS = "status";

}

