package com.dbadmaev.tschool.eCare.controller.client;

import com.dbadmaev.tschool.eCare.dto.client.ClientsRegistrationDto;
import com.dbadmaev.tschool.eCare.service.interfaces.clients.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/registration")
public class RegistrationController {

    private final ClientService clientService;

    @GetMapping
    public String getRegistration(Model model) {
        model.addAttribute("client", new ClientsRegistrationDto());
        return "/registration";
    }

    @PostMapping
    public String register(@ModelAttribute("client") @Valid ClientsRegistrationDto clientsRegistrationDto,
                           BindingResult bindingResult, Model model) {

        return clientService.registerClient(clientsRegistrationDto, bindingResult, model);
    }
}
