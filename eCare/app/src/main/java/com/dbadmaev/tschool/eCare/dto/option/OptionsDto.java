package com.dbadmaev.tschool.eCare.dto.option;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OptionsDto implements Serializable {
    private long id;

    @NotEmpty(message = "Option must have type")
    private String optionType;

    @NotEmpty(message = "Option must have a name")
    @Size(min = 2, max = 50, message = "name length should be 2-50!")
    private String name;

    @DecimalMin(value = "0.0", message = "Can't be empty, must be more than 0")
    @Digits(integer=15, fraction=2, message = "Can't be empty, 15 digits before and 2 digits after a point")
    private BigDecimal price;

    @DecimalMin(value = "0.0", message = "Can't be empty, must be more than 0")
    @Digits(integer=15, fraction=2, message = "Can't be empty, 15 digits before and 2 digits after a point")
    private BigDecimal connectionCost;

    private boolean active;

    private List<Long> incompatibleOptionsDtoList;

    private List<Long> linkedOptionsDtoList;
}
