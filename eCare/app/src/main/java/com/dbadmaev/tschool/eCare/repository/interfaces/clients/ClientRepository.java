package com.dbadmaev.tschool.eCare.repository.interfaces.clients;

import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.exceptions.ClientException;

import java.util.List;

/**
 * Client repository interface.
 *
 * @author Дмитрий
 */
public interface ClientRepository {
    /**
     * Find all clients.
     *
     * @exception ClientException if no clients found
     * @return List of Clients entities
     */
    List<Clients> findAll() throws ClientException;

    /**
     * Find client entity by id.
     *
     * @param id client's id
     * @exception ClientException if client is not found
     * @return Clients entity
     */
    Clients findById(long id) throws ClientException;

    /**
     * Find client entity by email.
     *
     * @param email client's email
     * @exception ClientException if client is not found
     * @return Clients entity
     */
    Clients findByEmail(String email) throws ClientException;

    /**
     * Find client entity by phone number.
     *
     * @param number client's phone number
     * @exception ClientException if client is not found
     * @return Clients entity
     */
    Clients findByNumber(String number) throws ClientException;

    /**
     * Add client entity.
     *
     * @param client Clients entity
     * @exception ClientException if client wasn't saved
     */
    void save(Clients client) throws ClientException;

    /**
     * Update client entity.
     *
     * @param client Clients entity
     * @exception ClientException if client wasn't updated
     */
    void update(Clients client) throws ClientException;
}
