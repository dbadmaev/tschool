package com.dbadmaev.tschool.eCare.mapper.option;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.enums.OptionType;
import com.dbadmaev.tschool.eCare.repository.interfaces.options.OptionRepository;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class OptionsMapper {

    private final OptionRepository optionRepository;
    private static Logger log = LoggerFactory.getLogger(OptionsMapper.class);

    public OptionsDto toDto (Options options) {

        log.info("Converting option to dto");

        OptionsDto optionsDto = new OptionsDto();

        optionsDto.setId(options.getId());

        if (options.getOptionType().equals(OptionType.INTERNET)) {
            optionsDto.setOptionType(OptionType.INTERNET.toString());

        } else if (options.getOptionType().equals(OptionType.CALLS)) {
            optionsDto.setOptionType(OptionType.CALLS.toString());

        } else {
            optionsDto.setOptionType(OptionType.SMS.toString());
        }
        optionsDto.setName(options.getName());
        optionsDto.setPrice(options.getPrice());
        optionsDto.setConnectionCost(options.getConnectionCost());
        optionsDto.setActive(options.isActive());

        List<Long> incompatibleOptionsDtoList = new ArrayList<>();
        if (options.getIncompatibleOptionsList() != null) {

            for (int i = 0; i < options.getIncompatibleOptionsList().size(); i++) {
                incompatibleOptionsDtoList.add(options.getIncompatibleOptionsList().get(i).getId());
            }
        }

        List<Long> linkedOptionsDtoList = new ArrayList<>();

        if (options.getLinkedOptionsList() != null) {
            for (int i = 0; i < options.getLinkedOptionsList().size(); i++) {
                linkedOptionsDtoList.add(options.getLinkedOptionsList().get(i).getId());
            }
        }
        return optionsDto;
    }

    public List<OptionsDto> toDtoList (List<Options> optionsList) {

        log.info("Converting option list to dto list");

        List<OptionsDto> optionsDtoList = new ArrayList<>();

        if (optionsList == null) {
            return null;
        }

        for (int i = 0; i < optionsList.size(); i++) {

            optionsDtoList.add(toDto(optionsList.get(i)));

        }
        return optionsDtoList;
    }
}
