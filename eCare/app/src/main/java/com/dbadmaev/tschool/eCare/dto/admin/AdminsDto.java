package com.dbadmaev.tschool.eCare.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdminsDto implements Serializable {
    private long id;
    private String name;
    private String password;
}
