package com.dbadmaev.tschool.eCare.repository.impl.contracts;

import com.dbadmaev.tschool.eCare.entity.Contracts;
import com.dbadmaev.tschool.eCare.entity.Contracts_;
import com.dbadmaev.tschool.eCare.exceptions.ContractException;
import com.dbadmaev.tschool.eCare.repository.interfaces.contracts.ContractRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ContractRepositoryImpl implements ContractRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Contracts> findAll() throws ContractException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Contracts> criteriaQuery = criteriaBuilder.createQuery(Contracts.class);
            Root<Contracts> root = criteriaQuery.from(Contracts.class);
            criteriaQuery
                    .select(root);
            TypedQuery<Contracts> contractsQuery = entityManager.createQuery(criteriaQuery);
            return contractsQuery.getResultList();
        } catch (PersistenceException e) {
            throw new ContractException("Error finding all contracts");
        }
    }

    @Override
    public Contracts findById(long id) throws ContractException {
        try {
            return entityManager.find(Contracts.class, id);
        } catch (PersistenceException e) {
            throw new ContractException("Error finding contract by id");
        }
    }

    @Override
    public List<Contracts> findByClientId(long clientId) throws ContractException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

            CriteriaQuery<Contracts> criteriaQuery = criteriaBuilder.createQuery(Contracts.class);
            Root<Contracts> root = criteriaQuery.from(Contracts.class);

            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Contracts_.client).get("id"), clientId));
            TypedQuery<Contracts> selectAll = entityManager.createQuery(criteriaQuery);

            return selectAll.getResultList();
        } catch (PersistenceException e) {
            throw new ContractException("Error finding contract by client id");
        }
    }

    @Override
    public Contracts findAvailableContract() throws ContractException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Contracts> criteriaQuery = criteriaBuilder.createQuery(Contracts.class);
            Root<Contracts> root = criteriaQuery.from(Contracts.class);

            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Contracts_.STATUS), "FREE"));
            TypedQuery<Contracts> findAvailableContract = entityManager.createQuery(criteriaQuery);

            return findAvailableContract.getResultStream().findFirst().orElse(null);
        } catch (PersistenceException e) {
            throw new ContractException("Error finding free contract");
        }
    }


    @Override
    public Contracts findLastContract() throws ContractException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Contracts> criteriaQuery = criteriaBuilder.createQuery(Contracts.class);
            Root<Contracts> root = criteriaQuery.from(Contracts.class);
            criteriaQuery
                    .select(root)
                    .orderBy(criteriaBuilder.desc(root.get("id")));
            TypedQuery<Contracts> findLastContract = entityManager.createQuery(criteriaQuery);
            return findLastContract.getResultStream().findFirst().orElse(null);
        } catch (PersistenceException e) {
            throw new ContractException("Error finding last created contract");
        }
    }

    @Override
    public void save(Contracts contract) throws ContractException {
        try {
            entityManager.persist(contract);
        } catch (PersistenceException e) {
            throw new ContractException("Error saving contract");
        }
    }

    @Override
    public void update(Contracts contract) throws ContractException {
        try {
            entityManager.merge(contract);
        } catch (PersistenceException e) {
            throw new ContractException("Error updating contract");
        }
    }
}
