package com.dbadmaev.tschool.eCare.controller.client;

import com.dbadmaev.tschool.eCare.dto.option.OptionBucketDto;
import com.dbadmaev.tschool.eCare.service.interfaces.contracts.ContractService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
@RequiredArgsConstructor
@RequestMapping("/contracts")
public class ContractsController {

    private final ContractService contractService;

    @GetMapping
    public String getAllContracts(Model model) {
        model.addAttribute("contracts", contractService.findAll());
        return "contracts";
    }

    @GetMapping("/edit/{id}")
    public String getEditContractPage(@PathVariable("id") long id,
                                      Model model,
                                      @SessionAttribute("bucket") ArrayList<OptionBucketDto> bucket){
        return contractService.getContractEditPage(id, model, bucket);
    }

    @PostMapping("/edit-tariff/{id}")
    public String setTariffForContract(@PathVariable("id") long id,
                                       @RequestParam("selected") long tariffId,
                                       @SessionAttribute("bucket") ArrayList<OptionBucketDto> bucket) {
        return contractService.setTariffForContract(id, tariffId, bucket);
    }

    @PostMapping("/edit-options/{id}")
    public String setOptionsForContract(@PathVariable("id") long id,
                                        @RequestParam("selected") long optionId,
                                        @SessionAttribute("bucket") ArrayList<OptionBucketDto> bucket) {
        return contractService.addOptionToBucket(id, optionId, bucket);
    }

    @PostMapping("/delete-options/{id}")
    public String deleteOptionsFromContract(@PathVariable("id") long id, Long[] selected) {
        return contractService.deleteOptionsFromContract(id, selected);
    }

    @GetMapping("/block/{id}")
    public String blockContract(@PathVariable("id") long id){
        return contractService.blockContract(id);
    }

    @GetMapping("/unblock/{id}")
    public String unblockContract(@PathVariable("id") long id){
        return contractService.unblockContract(id);
    }

}
