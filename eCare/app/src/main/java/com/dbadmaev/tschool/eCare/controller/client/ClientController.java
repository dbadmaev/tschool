package com.dbadmaev.tschool.eCare.controller.client;

import com.dbadmaev.tschool.eCare.service.interfaces.clients.ClientService;
import com.dbadmaev.tschool.eCare.service.interfaces.contracts.ContractService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/profile")
public class ClientController {

    private final ClientService clientService;
    private final ContractService contractService;

    @GetMapping
    public String getProfilePage(Model model){
        return clientService.getProfilePage(model);
    }

    @GetMapping("/create")
    public String createContract(Model model){
        return contractService.createContract();
    }


}
