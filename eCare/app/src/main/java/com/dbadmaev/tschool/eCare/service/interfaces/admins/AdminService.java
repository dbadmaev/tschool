package com.dbadmaev.tschool.eCare.service.interfaces.admins;

import org.springframework.ui.Model;

import java.util.List;

/**
 * Administrator service interface
 *
 * @author Дмитрий
 */
public interface AdminService {

    /**
     * Controller with blocking client by id functionality. Blocked client can't use eCare.
     *
     * @param userId Client id
     * @return redirect to jsp with all clients
     */
    String blockUser(long userId);

    /**
     * Controller with unblocking client by id functionality.
     *
     * @param userId Client id
     * @return redirect to jsp with all clients
     */
    String unblockUser(long userId);

    /**
     * Controller with blocking contracts by id functionality. Blocked contract can't be modified.
     *
     * @param contractId Contract id
     * @return redirect to jsp with all contracts
     */
    String blockContract(long contractId);

    /**
     * Controller with unblocking contracts by id functionality.
     *
     * @param contractId Contract id
     * @return redirect to jsp with all contracts
     */
    String unblockContract(long contractId);

    /**
     * Controller with editing contracts functionality.
     *
     * @param id Contract id
     * @param model Model receiving the required data
     * @return jsp with contracts edit data
     */
    String getEditContractPage(long id, Model model);

    /**
     * Controller with setting tariff for contract functionality.
     *
     * @param contractId Contract id
     * @param tariffId Tariff id
     * @return redirect to jsp with contracts and tariffs to choose
     */
    String setTariffForContract(long contractId, long tariffId);

    /**
     * Controller with adding option to contract functionality.
     * @param contractId Contract id
     * @param optionId Option id
     * @return redirect to jsp with contracts and options to add.
     */
    String setOptionForContract(long contractId, long optionId);

    /**
     * Controller with deleting options from chosen contract functionality.
     *
     * @param id Contract id
     * @param selected Array with chosen options ids
     * @return redirect to jsp with contracts and options to delete from the contract.
     */
    String deleteOptionsFromContract(long id, Long[] selected);

    /**
     * Controller with looking for client by number.
     *
     * @param number Client phone number
     * @param model Model receiving the required data
     * @return jsp with search by number field
     */
    String findClientByNumber(String number, Model model);

    /**
     * Find all phone numbers.
     *
     * @return List with strings of numbers
     */
    List<String> getNumbers();

    /**
     *
     * @param id Option id
     * @return redirect to jsp with all options
     */
    String changeOptionStatus(long id);
}
