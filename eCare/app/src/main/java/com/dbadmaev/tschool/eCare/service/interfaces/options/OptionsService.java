package com.dbadmaev.tschool.eCare.service.interfaces.options;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.entity.Options;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * Option service interface.
 *
 * @author Дмитрий
 */
public interface OptionsService {

    /**
     * Find all options entities.
     *
     * @return List of options entities
     */
    List<Options> findAll();

    /**
     * Find option by id.
     *
     * @param id Option id
     * @return Options entity
     */
    Options findById(long id);

    /**
     * Find option by name.
     *
     * @param name Option name
     * @return Options entity
     */
    Options findByName(String name);

    /**
     * Find all options entities ordered by its types put into DTOs.
     *
     * @param isActive Boolean flag showing is option active
     * @return List of archive or active options entities put into DTOs
     */
    List<OptionsDto> orderedFindAll(boolean isActive);

    /**
     * Find all types of option.
     *
     * @return List of strings of types of option
     */
    List<String> getOptionTypes ();

    /**
     * Controller with editing incompatible options for option.
     *
     * @param id Option id
     * @param selected Array of IDs of chosen incompatible options
     * @param model Model receiving required data
     * @return redirect to jsp with option editing
     */
    String editIncompatibleOptions(long id, Long[] selected, Model model);

    /**
     * Controller with editing linked options for option.
     *
     * @param id Option id
     * @param selected Array of IDs of chosen linked options
     * @param model Model receiving required data
     * @return redirect to jsp with option editing
     */
    String editLinkedOptions(long id, Long[] selected, Model model);

    /**
     * Getting all available options to be incompatible for chosen option.
     *
     * @param id Chosen option id
     * @return List of incompatible options entities
     */
    List<Options> getAvailableToAddIncompatibleOptions(long id);

    /**
     * Getting all available options to be linked with chosen option.
     *
     * @param id Chosen option id
     * @return List of linked options entities
     */
    List<Options> getAvailableToAddLinkedOptions(long id);

    /**
     * Controller with editing incompatible options functionality.
     *
     * @param id Option id
     * @param model Model receiving required data
     * @return jsp with editing incompatible options
     */
    String getIncompatibleEditForm(long id, Model model);

    /**
     * Controller with editing lniked options functionality.
     *
     * @param id Option id
     * @param model Model receiving required data
     * @return jsp with editing lniked options
     */
    String getLinkedEditForm(long id, Model model);

    /**
     * Controller with option editing functionality: redirect to edit incompatible or linked options.
     *
     * @param id Edited option id
     * @param model Model receiving required data
     * @return jsp with available incompatible and linked options
     */
    String editOption(long id, Model model);

    /**
     * Controller with creating option functionality.
     *
     * @param optionsDto New option DTO
     * @param model Model receiving required data
     * @param bindingResult Binding result
     * @return redirect to jsp with option creating page
     */
    String createOption(OptionsDto optionsDto, Model model, BindingResult bindingResult);

    /**
     * Check if List of options entities is empty.
     *
     * @param optionsList List of options entities
     * @return true if empty, false if not empty
     */
    boolean empty(List<Options> optionsList);

    /**
     * Adding option entity by option DTO.
     *
     * @param optionsDto Option DTO
     */
    void save(OptionsDto optionsDto);
}
