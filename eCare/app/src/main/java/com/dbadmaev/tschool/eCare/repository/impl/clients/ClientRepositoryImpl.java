package com.dbadmaev.tschool.eCare.repository.impl.clients;

import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.entity.Clients_;
import com.dbadmaev.tschool.eCare.entity.Contracts;
import com.dbadmaev.tschool.eCare.entity.Contracts_;
import com.dbadmaev.tschool.eCare.exceptions.ClientException;
import com.dbadmaev.tschool.eCare.repository.interfaces.clients.ClientRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ClientRepositoryImpl implements ClientRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Clients> findAll() throws ClientException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Clients> criteriaQuery = criteriaBuilder.createQuery(Clients.class);
            Root<Clients> root = criteriaQuery.from(Clients.class);

            criteriaQuery
                    .select(root);
            TypedQuery<Clients> clientsQuery = entityManager.createQuery(criteriaQuery);
            return clientsQuery.getResultList();
        } catch (PersistenceException e) {
            throw new ClientException("Error getting all clients");
        }
    }

    @Override
    public Clients findById(long id) throws ClientException {
        try {
            return entityManager.find(Clients.class, id);
        } catch (PersistenceException e) {
            throw new ClientException("Error finding client by id");
        }
    }

    @Override
    public Clients findByEmail(String email) throws ClientException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Clients> criteriaQuery = criteriaBuilder.createQuery(Clients.class);
            Root<Clients> root = criteriaQuery.from(Clients.class);

            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Clients_.email), email));
            TypedQuery<Clients> selectByEmail = entityManager.createQuery(criteriaQuery);

            return selectByEmail.getResultStream().findFirst().orElse(null);
        } catch (PersistenceException e) {
            throw new ClientException("Error finding user by email");
        }
    }

    @Override
    public Clients findByNumber(String number) throws ClientException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Clients> criteriaQuery = criteriaBuilder.createQuery(Clients.class);
            Root<Clients> root = criteriaQuery.from(Clients.class);

            Join<Clients, Contracts> clientsContractsJoin = root.join(Clients_.contractsList);

            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(clientsContractsJoin.get(Contracts_.number), number));
            TypedQuery<Clients> selectByNumber = entityManager.createQuery(criteriaQuery);

            return selectByNumber.getResultStream().findFirst().orElse(null);
        } catch (PersistenceException e) {
            throw new ClientException("Error finding user by number");
        }
    }

    @Override
    public void save(Clients client) throws ClientException {
        try {
            entityManager.persist(client);
        } catch (PersistenceException e) {
            throw new ClientException("Error saving client into database");
        }
    }

    @Override
    public void update(Clients client) throws ClientException {
        try {
            entityManager.merge(client);
        } catch (PersistenceException e) {
            throw new ClientException("Error updating client in database");
        }
    }
}
