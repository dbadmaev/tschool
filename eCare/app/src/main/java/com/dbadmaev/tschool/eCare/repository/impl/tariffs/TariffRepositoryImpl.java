package com.dbadmaev.tschool.eCare.repository.impl.tariffs;

import com.dbadmaev.tschool.eCare.entity.Tariffs;
import com.dbadmaev.tschool.eCare.entity.Tariffs_;
import com.dbadmaev.tschool.eCare.exceptions.TariffException;
import com.dbadmaev.tschool.eCare.repository.interfaces.tariffs.TariffRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class TariffRepositoryImpl implements TariffRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Tariffs> findAllActive() throws TariffException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Tariffs> criteriaQuery = criteriaBuilder.createQuery(Tariffs.class);
            Root<Tariffs> root = criteriaQuery.from(Tariffs.class);
            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Tariffs_.status), 1));
            TypedQuery<Tariffs> tariffsQuery = entityManager.createQuery(criteriaQuery);
            return tariffsQuery.getResultList();
        } catch (PersistenceException e) {
            throw new TariffException("Error finding all active tariffs");
        }
    }

    @Override
    public List<Tariffs> findAllArchive() throws TariffException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Tariffs> criteriaQuery = criteriaBuilder.createQuery(Tariffs.class);
            Root<Tariffs> root = criteriaQuery.from(Tariffs.class);
            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Tariffs_.status), 0));
            TypedQuery<Tariffs> tariffsQuery = entityManager.createQuery(criteriaQuery);
            return tariffsQuery.getResultList();
        } catch (PersistenceException e) {
            throw new TariffException("Error finding all archive tariffs");
        }
    }

    @Override
    public Tariffs findById(long id) throws TariffException {
        try {
            return entityManager.find(Tariffs.class, id);
        } catch (PersistenceException e) {
            throw new TariffException("Error finding tariff by id");
        }
    }

    @Override
    public void save(Tariffs tariff) throws TariffException {
        try {
            entityManager.persist(tariff);
        } catch (PersistenceException e) {
            throw new TariffException("Error saving tariff");
        }
    }

    @Override
    public Tariffs findByName(String name) throws TariffException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Tariffs> criteriaQuery = criteriaBuilder.createQuery(Tariffs.class);
            Root<Tariffs> root = criteriaQuery.from(Tariffs.class);

            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Tariffs_.name), name));
            TypedQuery<Tariffs> findOptionByName = entityManager.createQuery(criteriaQuery);

            return findOptionByName.getResultStream().findFirst().orElse(null);
        } catch (PersistenceException e) {
            throw new TariffException("Error finding tariff by name");
        }
    }

    @Override
    public void update(Tariffs tariff) throws TariffException {
        try {
            entityManager.merge(tariff);
        } catch (PersistenceException e) {
            throw new TariffException("Error updating tariff");
        }
    }
}
