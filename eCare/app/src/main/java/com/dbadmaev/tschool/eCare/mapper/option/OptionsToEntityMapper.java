package com.dbadmaev.tschool.eCare.mapper.option;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.enums.OptionType;
import com.dbadmaev.tschool.eCare.exceptions.OptionException;
import com.dbadmaev.tschool.eCare.repository.interfaces.options.OptionRepository;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class OptionsToEntityMapper {

    private final OptionRepository optionRepository;
    private static Logger log = LoggerFactory.getLogger(OptionsToEntityMapper.class);

    public Options dtoToEntity(OptionsDto optionsDto) {
        log.info("converting optionDto(" + optionsDto.getId() + ") to entity");

        Options option = new Options();
        option.setActive(true);

        if (optionsDto.getOptionType().equals(OptionType.INTERNET.toString())) {
            option.setOptionType(OptionType.INTERNET);

        } else if (optionsDto.getOptionType().equals(OptionType.CALLS.toString())) {
            option.setOptionType(OptionType.CALLS);

        } else {
            option.setOptionType(OptionType.SMS);
        }

        option.setName(optionsDto.getName());
        option.setPrice(optionsDto.getPrice());
        option.setConnectionCost(optionsDto.getConnectionCost());
        option.setActive(true);

        List<Options> incompatibleOptionsList = new ArrayList<>();
        if (optionsDto.getIncompatibleOptionsDtoList() != null) {

            for (int i = 0; i < optionsDto.getIncompatibleOptionsDtoList().size(); i++) {
                try {
                    incompatibleOptionsList.add(optionRepository.findById(optionsDto.getIncompatibleOptionsDtoList().get(i)));
                } catch (OptionException e) {
                    log.error("Error finding option(" + optionsDto.getIncompatibleOptionsDtoList().get(i) + ")", e);
                } catch (Exception e) {
                    log.error("Error in dtoToEntity()", e);
                }
            }
        }

        List<Options> linkedOptionsList = new ArrayList<>();

        if (optionsDto.getLinkedOptionsDtoList() != null) {
            for (int i = 0; i < optionsDto.getLinkedOptionsDtoList().size(); i++) {
                try {
                    linkedOptionsList.add(optionRepository.findById(optionsDto.getLinkedOptionsDtoList().get(i)));
                } catch (OptionException e) {
                    log.error("Error finding option(" + optionsDto.getLinkedOptionsDtoList().get(i) + ")", e);
                } catch (Exception e) {
                    log.error("Error in dtoToEntity()", e);
                }
            }
        }
        return option;
    }
}
