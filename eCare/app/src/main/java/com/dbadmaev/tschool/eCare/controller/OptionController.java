package com.dbadmaev.tschool.eCare.controller;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.service.interfaces.options.OptionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/options")
public class OptionController {

    private final OptionsService optionsService;

    @GetMapping
    public String getOptionPage(Model model){
        model.addAttribute("options", optionsService.orderedFindAll(true));
        return "options";
    }

    @GetMapping("/create")
    private String getAddForm(Model model) {
        model.addAttribute("createOption", new OptionsDto());
        model.addAttribute("optionTypes", optionsService.getOptionTypes());
        return "option-create";
    }

    @PostMapping("/create")
    public String addOption(@ModelAttribute("createOption") @Valid OptionsDto optionsDto, Model model,
                            BindingResult bindingResult) {
        return optionsService.createOption(optionsDto, model, bindingResult);
    }

    @GetMapping("/edit/{id}")
    public String editOption(@PathVariable("id") long id, Model model) {
        return optionsService.editOption(id, model);
    }

    @GetMapping("/edit-incompatible/{id}")
    public String editIncompatibleOptions(@PathVariable("id") long id, Model model) {
        return optionsService.getIncompatibleEditForm(id, model);
    }

    @PostMapping("/edit-incompatible/{id}")
    public String getEditIncompatibleOptions(@PathVariable("id") long id, Long[] selected, Model model) {
       return optionsService.editIncompatibleOptions(id, selected, model);
    }

    @GetMapping("/edit-linked/{id}")
    public String editLinkedOptions(@PathVariable("id") long id, Model model) {
        return optionsService.getLinkedEditForm(id, model);
    }

    @PostMapping("/edit-linked/{id}")
    public String getEditLinkedOptions(@PathVariable("id") long id, Long[] selected, Model model) {
        return optionsService.editLinkedOptions(id, selected, model);
    }



}