package com.dbadmaev.tschool.eCare.dto.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientsRegistrationDto implements Serializable {

    private String role = "USER";

    @NotEmpty(message = "First name should not be empty")
    @Size(min = 2, max = 50, message = "name length should be 2-50!")
    private String name;

    @NotEmpty(message = "Last name should not be empty")
    @Size(min = 2, max = 50, message = "Last name should be 2-50!")
    private String secondName;

    @NotEmpty(message = "Date of birth should not be empty")
    private String dateOfBirth;

    @NotEmpty(message = "Passport data should not be empty")
    private String passportData;

    @NotEmpty(message = "Address should not be empty")
    private String address;

    @NotEmpty(message = "Email should not be empty")
    @Email(message = "Email should be valid")
    private String email;

    @NotEmpty(message = "Confirm email should not be empty")
    @Email(message = "Email should be valid")
    private String confirmEmail;

    @NotEmpty(message = "Password should not be empty")
    private String password;

    @NotEmpty(message = "Confirm password should not be empty")
    private String confirmPassword;
}
