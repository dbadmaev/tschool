package com.dbadmaev.tschool.eCare.exceptions;

public class BucketException extends Exception {
    public BucketException(String message) {
        super(message);
    }

    public BucketException(String message, Throwable cause) {
        super(message, cause);
    }
}
