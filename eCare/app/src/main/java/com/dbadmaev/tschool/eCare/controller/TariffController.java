package com.dbadmaev.tschool.eCare.controller;

import com.dbadmaev.tschool.eCare.dto.tariff.TariffsDto;
import com.dbadmaev.tschool.eCare.service.impl.MessageSender;
import com.dbadmaev.tschool.eCare.service.interfaces.tariffs.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/tariffs")
public class TariffController {

    private final TariffService tariffService;
    private final MessageSender messageSender;

    @GetMapping
    public String getAllTariffs(Model model){
        model.addAttribute("tariffs", tariffService.getTariffsList());
        return "tariffs";
    }

    @GetMapping("/get/{id}")
    public String getAllTariff(@PathVariable("id") long id, Model model){
        model.addAttribute("tariff", tariffService.getTariff(id));
        return "tariff";
    }

    @GetMapping("/create")
    public String createTariff(Model model){
        model.addAttribute("createTariff", new TariffsDto());
        return "tariff-create";
    }

    @PostMapping("/create")
    public String addTariff(@ModelAttribute("createTariff") @Valid TariffsDto tariffsDto, Model model,
                            BindingResult bindingResult) {
        messageSender.sendMessage();
        return tariffService.createTariff(tariffsDto, model, bindingResult);
    }

    @GetMapping("/delete/{id}")
    public String deleteTariff(@PathVariable("id") long id){
        tariffService.deleteTariff(id);
        messageSender.sendMessage();
        return "redirect:/tariffs";
    }

    @GetMapping("/edit/{id}")
    public String editTariff(@PathVariable("id") long id, Model model){
        return tariffService.getAvailableOptions(id, model);
    }

    @PostMapping("/edit/{id}")
    public String getEditTariff(@PathVariable("id") long id, @RequestParam("selected") long optId) {
        return tariffService.addOption(id, optId);
    }

    @PostMapping("/delete/{id}")
    public String deleteOption(@PathVariable("id") long id, @RequestParam("selected") long optId) {
        return tariffService.deleteOption(id, optId);
    }

}
