package com.dbadmaev.tschool.eCare.repository.impl.admins;

import com.dbadmaev.tschool.eCare.entity.Admins;
import com.dbadmaev.tschool.eCare.entity.Admins_;
import com.dbadmaev.tschool.eCare.exceptions.AdminException;
import com.dbadmaev.tschool.eCare.repository.interfaces.admins.AdminRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;
@Repository
@Transactional
public class AdminRepositoryImpl implements AdminRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Admins> findAll() throws AdminException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Admins> criteriaQuery = criteriaBuilder.createQuery(Admins.class);
            Root<Admins> root = criteriaQuery.from(Admins.class);
            criteriaQuery
                    .select(root);
            TypedQuery<Admins> adminsQuery = entityManager.createQuery(criteriaQuery);
            return adminsQuery.getResultList();
        } catch (PersistenceException e) {
            throw new AdminException("Error getting all admins");
        }
    }

    @Override
    public Admins findById(long id) throws AdminException {
        try {
            return entityManager.find(Admins.class, id);
        } catch (PersistenceException e) {
            throw new AdminException("Error finding admin by id");
        }
    }

    @Override
    public Admins findByName(String name) throws AdminException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Admins> criteriaQuery = criteriaBuilder.createQuery(Admins.class);
            Root<Admins> root = criteriaQuery.from(Admins.class);
            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Admins_.name), name));
            TypedQuery<Admins> selectByName = entityManager.createQuery(criteriaQuery);

            return selectByName.getResultStream().findFirst().orElse(null);
        } catch (PersistenceException e) {
            throw new AdminException("Error finding admin by name");
        }
    }

    @Override
    public void save(Admins admin) throws AdminException {
        try {
            entityManager.persist(admin);
        } catch (PersistenceException e) {
            throw new AdminException("Error saving admin into database");
        }
    }

    @Override
    public void update(Admins admin) throws AdminException {
        try {
            entityManager.merge(admin);
        } catch (PersistenceException e) {
            throw new AdminException("Error updating admin in database");
        }
    }
}
