package com.dbadmaev.tschool.eCare.repository.interfaces.options;

import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.exceptions.OptionException;

import java.util.List;

/**
 * Option repository interface.
 *
 * @author Дмитрий
 */
public interface OptionRepository {

    /**
     * Find all options.
     *
     * @exception OptionException if no options found.
     * @return List of all options
     */
    List<Options> findAll() throws OptionException;

    /**
     * Find option by id.
     *
     * @param id Options id
     * @exception OptionException if no options found
     * @return Options entity
     */
    Options findById(long id) throws OptionException;

    /**
     * Find option by name.
     *
     * @param name Options name
     * @exception OptionException if no options found
     * @return Options entity
     */
    Options findByName(String name) throws OptionException;

    /**
     * Find all options with requested type.
     *
     * @param isActive Boolean flag: if 1 looks for active options, if 0 looks for archive options
     * @exception OptionException if no options found.
     * @return List of option entities
     */
    List<Options> findOptionsByType(boolean isActive) throws OptionException;

    /**
     * Find options included in tariff.
     *
     * @param tariffId Tariff id
     * @exception OptionException if no options found.
     * @return List of option entities
     */
    List<Options> findPossibleTariffOptions(long tariffId) throws OptionException;

    /**
     * Add option entity.
     *
     * @param option Option entity
     * @exception OptionException if option wasn't saved
     */
    void save(Options option) throws OptionException;

    /**
     * Update option entity.
     *
     * @param option Option entity
     * @exception OptionException if option wasn't updated
     */
    void update(Options option) throws OptionException;

    /**
     * Find all active options.
     *
     * @exception OptionException if no options found
     * @return List of option entities
     */
    List<Options> findAllActive() throws OptionException;

    /**
     * Find all archive options.
     *
     * @exception OptionException if no options found
     * @return List of option entities
     */
    List<Options> findAllArchived() throws OptionException;
}
