package com.dbadmaev.tschool.eCare.service.impl.clients;

import com.dbadmaev.tschool.eCare.dto.client.ClientsDto;
import com.dbadmaev.tschool.eCare.dto.client.ClientsRegistrationDto;
import com.dbadmaev.tschool.eCare.dto.contract.ContractsDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.exceptions.ClientException;
import com.dbadmaev.tschool.eCare.mapper.client.ClientsMapper;
import com.dbadmaev.tschool.eCare.mapper.client.ClientsRegistrationMapper;
import com.dbadmaev.tschool.eCare.repository.interfaces.clients.ClientRepository;
import com.dbadmaev.tschool.eCare.service.interfaces.clients.ClientService;
import com.dbadmaev.tschool.eCare.service.interfaces.contracts.ContractService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final ClientsMapper clientsMapper;
    private final ClientsRegistrationMapper clientsRegistrationMapper;
    private final PasswordEncoder bCryptPasswordEncoder;
    private final ContractService contractService;
    private static Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Override
    public List<ClientsDto> findAll() {
        log.info("Finding all clients in findAll()");
        List<ClientsDto> clientsDtoList = null;
        try {
            clientsDtoList = clientsMapper.toDtoList(clientRepository.findAll());
        } catch (ClientException e) {
            log.error("Error finding all clients", e);
        } catch (Exception e) {
            log.error("Error in ClientService.findAll()", e);
        }
        return clientsDtoList;
    }

    @Override
    public ClientsDto findById(long id) {
        log.info("Finding client by id in findById()");
        ClientsDto clientsDto = null;
        try {
            clientsDto = clientsMapper.toDto(clientRepository.findById(id));
        } catch (ClientException e) {
            log.error("Error finding client by id", e);
        } catch (Exception e) {
            log.error("Error in ClientService.findById()", e);
        }
        return clientsDto;
    }

    @Override
    @Transactional
    public String registerClient(ClientsRegistrationDto clientsRegistrationDto, BindingResult bindingResult, Model model) {
        log.info("Getting register client info in registerClient()");
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        try {
            if (clientRepository.findByEmail(clientsRegistrationDto.getEmail()) != null) {
                model.addAttribute("userError", "User with this email is already registered");
                return "registration";
            }
        } catch (ClientException e) {
            log.error("Error finding client by email", e);
        } catch (Exception e) {
            log.error("Error in registerClient()", e);
        }

        if (!clientsRegistrationDto.getEmail().equals(clientsRegistrationDto.getConfirmEmail())) {
            model.addAttribute("emailError", "Email does not match");
            return "registration";
        }

        if (!clientsRegistrationDto.getPassword().equals(clientsRegistrationDto.getConfirmPassword())) {
            model.addAttribute("passwordError", "Password does not match");
            return "registration";
        }
        clientsRegistrationDto.setPassword(bCryptPasswordEncoder.encode(clientsRegistrationDto.getPassword()));
        save(clientsRegistrationDto);
        return "redirect:/";
    }

    @Override
    public void save(ClientsRegistrationDto clientsRegistrationDto) {
        log.info("Saving client in save()");
        try {
            clientRepository.save(clientsRegistrationMapper.dtoToEntity(clientsRegistrationDto));
        } catch (ClientException e) {
            log.error("Error saving client", e);
        } catch (Exception e) {
            log.error("Error in ClientService.save()", e);
        }
    }

    @Override
    public void update(ClientsDto clientsDto) {
        log.info("Updating client in update()");
        try {
            clientRepository.update(clientsMapper.dtoToEntity(clientsDto));
        } catch (ClientException e) {
            log.error("Error updating client", e);
        } catch (Exception e) {
            log.error("Error in ClientService.update()", e);
        }
    }

    @Override
    @Transactional
    public String getProfilePage(Model model) {
        log.info("Rendering profile page in getProfilePage()");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUser = authentication.getName();
        try {
            Clients clients = clientRepository.findByEmail(currentUser);
            List<ContractsDto> contracts = contractService.findContractsByClientId(clients.getId());
            model.addAttribute("contracts", contracts);
            model.addAttribute("contractsNull", contracts.isEmpty());
        } catch (ClientException e) {
            log.error("Error finding client by email", e);
        } catch (Exception e) {
            log.error("Error in ClientService.getProfilePage()", e);
        }
        return "profile";
    }
}
