package com.dbadmaev.tschool.eCare.mapper.tariff;

import com.dbadmaev.tschool.eCare.dto.tariff.TariffsDto;
import com.dbadmaev.tschool.eCare.entity.Tariffs;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TariffsMapper {
    TariffsMapper TARIFF_MAPPER = Mappers.getMapper(TariffsMapper.class);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "status", target = "status")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "price", target = "price")
    TariffsDto toDto(Tariffs tariffs);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "status", target = "status")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "price", target = "price")
    List<TariffsDto> toDtoList(List<Tariffs> tariffsList);
}
