package com.dbadmaev.tschool.eCare.dto.stand;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.dto.tariff.TariffsDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TariffWithOptionsDto {
    private TariffsDto tariffsDto;
    private List<OptionsDto> OptionsDto;
}
