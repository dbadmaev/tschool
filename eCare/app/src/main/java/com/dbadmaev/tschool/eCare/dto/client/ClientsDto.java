package com.dbadmaev.tschool.eCare.dto.client;

import com.dbadmaev.tschool.eCare.dto.contract.ContractsDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientsDto implements Serializable {
    private long id;
    private int blocked;
    private String name;
    private String secondName;
    private LocalDate dateOfBirth;
    private String passportData;
    private String address;
    private String email;
    private List<ContractsDto> contractsDtoList;
}
