package com.dbadmaev.tschool.eCare.controller;

import com.dbadmaev.tschool.eCare.service.impl.MessageSender;
import com.dbadmaev.tschool.eCare.service.interfaces.admins.AdminService;
import com.dbadmaev.tschool.eCare.service.interfaces.clients.ClientService;
import com.dbadmaev.tschool.eCare.service.interfaces.options.OptionsService;
import com.dbadmaev.tschool.eCare.service.interfaces.tariffs.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {

    private final ClientService clientService;
    private final TariffService tariffService;
    private final AdminService adminService;
    private final OptionsService optionsService;
    private final MessageSender messageSender;

    @GetMapping
    public String adminPage(Model model) {
        model.addAttribute("number", adminService.getNumbers());
        return "adminPage";
    }

    @GetMapping("/allClients")
    public String findAll(Model model) {
        model.addAttribute("clients", clientService.findAll());
        return "allClients";
    }
    @GetMapping("/archive")
    public String getArchive(Model model) {
        model.addAttribute("options", optionsService.orderedFindAll(false));
        model.addAttribute("tariffs", tariffService.findAllArchive());
        return "archive-page";
    }

    @GetMapping("/find")
    public String findById(@RequestParam("clientId") long clientId, Model model){
        model.addAttribute("client", clientService.findById(clientId));
        return "findClient";
    }

    @PostMapping("/findByNumber")
    public String findByNumber(@RequestParam("number") String number, Model model) {
        return adminService.findClientByNumber(number, model);
    }

    @GetMapping("/block-user/{id}")
    public String blockUser(@PathVariable("id") long id){
        return adminService.blockUser(id);
    }

    @GetMapping("/unblock-user/{id}")
    public String unblockUser(@PathVariable("id") long id){
        return adminService.unblockUser(id);
    }

    @GetMapping("/block-contract/{id}")
    public String blockContract(@PathVariable("id") long id){
        return adminService.blockContract(id);
    }

    @GetMapping("/unblock-contract/{id}")
    public String unblockContract(@PathVariable("id") long id){
        return adminService.unblockContract(id);
    }

    @GetMapping("/edit-contract/{id}")
    public String editContract(@PathVariable("id") long id, Model model){
        return adminService.getEditContractPage(id, model);
    }

    @PostMapping("/edit-tariff/{id}")
    public String setTariffForContract(@PathVariable("id") long id, @RequestParam("selected") long tariffId) {
        return adminService.setTariffForContract(id, tariffId);
    }

    @PostMapping("/edit-options/{id}")
    public String setOptionsForContract(@PathVariable("id") long id, @RequestParam("selected") long optionId) {
        return adminService.setOptionForContract(id, optionId);
    }

    @PostMapping("/delete-options/{id}")
    public String deleteOptionsFromContract(@PathVariable("id") long id, Long[] selected) {
        return adminService.deleteOptionsFromContract(id, selected);
    }

    @GetMapping("/change-status/{id}")
    public String changeOptionStatus(@PathVariable("id") long id) {
        return adminService.changeOptionStatus(id);
    }
}