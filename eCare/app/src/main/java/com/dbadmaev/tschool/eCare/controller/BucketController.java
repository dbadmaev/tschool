package com.dbadmaev.tschool.eCare.controller;

import com.dbadmaev.tschool.eCare.dto.option.OptionBucketDto;
import com.dbadmaev.tschool.eCare.service.interfaces.BucketService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.ArrayList;

@Controller
@RequiredArgsConstructor
@RequestMapping("/bucket")
public class BucketController {

    private final BucketService bucketService;

    @GetMapping
    public String getBucketPage(Model model,
                                @SessionAttribute("bucket") ArrayList<OptionBucketDto> bucket){
        return bucketService.getBucketView(model, bucket);
    }

    @PostMapping("/buy")
    public String buyOptions(Model model,
                             @SessionAttribute("bucket") ArrayList<OptionBucketDto> bucket,
                             Long[] selected){
        return bucketService.addOptionsToContract(model, bucket, selected);
    }
}
