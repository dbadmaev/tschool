package com.dbadmaev.tschool.eCare.dto.option;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OptionBucketDto {
    long contractId;
    List<Long> optionsIdList;
}
