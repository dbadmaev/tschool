package com.dbadmaev.tschool.eCare.exceptions;

public class OptionException extends Exception {
    public OptionException(String message) {
        super(message);
    }

    public OptionException(String message, Throwable cause) {
        super(message, cause);
    }
}
