package com.dbadmaev.tschool.eCare.controller;

import com.dbadmaev.tschool.eCare.dto.option.OptionBucketDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
@RequiredArgsConstructor
public class HomeController {

    @GetMapping("/")
    public String getHomePage(HttpSession httpSession){
        if (httpSession.getAttribute("bucket") == null) {
            httpSession.setAttribute("bucket", new ArrayList<OptionBucketDto>());
        }
        return "homePage";
    }

    @GetMapping("/403")
    public String accessDenied(){
        return "404";
    }
    @GetMapping("/404")
    public String pageNotFound(){
        return "404";
    }
}
