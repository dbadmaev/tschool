package com.dbadmaev.tschool.eCare.repository.interfaces.tariffs;

import com.dbadmaev.tschool.eCare.entity.Tariffs;
import com.dbadmaev.tschool.eCare.exceptions.TariffException;

import java.util.List;

/**
 * Tariff repository interface.
 *
 * @author Дмитрий
 */
public interface TariffRepository {

    /**
     * Find all active tariffs.
     *
     * @exception TariffException if no tariffs found
     * @return List of tariff entities
     */
    List<Tariffs> findAllActive() throws TariffException;

    /**
     * Find all archive tariffs.
     *
     * @exception TariffException if no tariffs found
     * @return List of tariff entities
     */
    List<Tariffs> findAllArchive() throws TariffException;

    /**
     * Find tariff entity by id.
     *
     * @param id Tariffs id
     * @exception TariffException if tariff wasn't found
     * @return Tariffs entity
     */
    Tariffs findById(long id) throws TariffException;

    /**
     * Add tariff entity.
     *
     * @param tariff Tariffs entity
     * @exception TariffException if tariff wasn't saved
     */
    void save(Tariffs tariff) throws TariffException;

    /**
     * Update tariff entity.
     *
     * @param tariff Tariffs entity
     * @exception TariffException if tariff wasn't updated
     */
    void update(Tariffs tariff) throws TariffException;

    /**
     * Find tariff entity by name.
     *
     * @param name Tariff name
     * @exception TariffException if tariff wasn't found
     * @return Tariffs entity
     */
    Tariffs findByName(String name) throws TariffException;
}
