package com.dbadmaev.tschool.eCare.service.impl.admins;

import com.dbadmaev.tschool.eCare.dto.client.ClientsDto;
import com.dbadmaev.tschool.eCare.dto.contract.ContractsDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.entity.Contracts;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.enums.ContractStatus;
import com.dbadmaev.tschool.eCare.exceptions.AdminException;
import com.dbadmaev.tschool.eCare.exceptions.ClientException;
import com.dbadmaev.tschool.eCare.exceptions.ContractException;
import com.dbadmaev.tschool.eCare.exceptions.OptionException;
import com.dbadmaev.tschool.eCare.mapper.admin.AdminsMapper;
import com.dbadmaev.tschool.eCare.mapper.client.ClientsMapper;
import com.dbadmaev.tschool.eCare.repository.interfaces.options.OptionRepository;
import com.dbadmaev.tschool.eCare.repository.interfaces.admins.AdminRepository;
import com.dbadmaev.tschool.eCare.repository.interfaces.clients.ClientRepository;
import com.dbadmaev.tschool.eCare.repository.interfaces.contracts.ContractRepository;
import com.dbadmaev.tschool.eCare.service.interfaces.admins.AdminService;
import com.dbadmaev.tschool.eCare.service.interfaces.contracts.ContractService;
import com.dbadmaev.tschool.eCare.service.interfaces.options.OptionsService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

    private final AdminRepository adminRepository;
    private final AdminsMapper adminsMapper;
    private final ClientRepository clientRepository;
    private final ClientsMapper clientsMapper;
    private final ContractRepository contractRepository;
    private final ContractService contractService;
    private final OptionRepository optionRepository;
    private static Logger log = LoggerFactory.getLogger(AdminServiceImpl.class);

    private final int BLOCKED = 1;
    private final int NOT_BLOCKED = 0;

//    @Override
//    public AdminsDto findById(long id) {
//        log.info("Finding admin by id in findById()");
//        AdminsDto adminsDto = null;
//        try {
//            adminsDto = adminsMapper.toDto(adminRepository.findById(id));
//        } catch (AdminException e) {
//            log.error("Error getting admin by id", e);
//        } catch (Exception e) {
//            log.error("Error in AdminService.findById()", e);
//        }
//        return adminsDto;
//    }
//
//    @Override
//    public AdminsDto findByName(String name) {
//        log.info("Finding admin by name in findByName()");
//        AdminsDto adminsDto = null;
//        try {
//            adminsDto = adminsMapper.toDto(adminRepository.findByName(name));
//        } catch (AdminException e) {
//            log.error("Error finding admin by name", e);
//        } catch (Exception e) {
//            log.error("Error in AdminService.findByName()", e);
//        }
//        return adminsDto;
//    }

    @Override
    @Transactional
    public String blockUser(long userId) {
        log.info("Blocking user by id in blockUser()");
        try {
            Clients clients = clientRepository.findById(userId);
            clients.setBlocked(BLOCKED);
            clientRepository.update(clients);
        } catch (ClientException e) {
            log.error("Error blocking client by admin", e);
        } catch (Exception e) {
            log.error("Error in AdminService.blockUser()", e);
        }
        return "redirect:/admin/allClients";
    }

    @Override
    @Transactional
    public String unblockUser(long userId) {
        log.info("Unblocking user in unblockUser()");
        try {
            Clients clients = clientRepository.findById(userId);
            clients.setBlocked(NOT_BLOCKED);
            clientRepository.update(clients);
        } catch (ClientException e) {
            log.error("Error unblocking client by admin", e);
        } catch (Exception e) {
            log.error("Error in AdminService.unblockUser()", e);
        }
        return "redirect:/admin/allClients";
    }

    @Override
    @Transactional
    public String blockContract(long contractId) {
        log.info("Blocking contract in blockContract()");
        try {
            Contracts contracts = contractRepository.findById(contractId);
            contracts.setStatus(ContractStatus.BLOCKED_BY_ADMIN);
            contractRepository.update(contracts);
        } catch (ContractException e) {
            log.error("Error blocking contract by admin", e);
        } catch (Exception e) {
            log.error("Error in AdminService.blockContract", e);
        }
        return "redirect:/contracts";
    }

    @Override
    @Transactional
    public String unblockContract(long contractId) {
        log.info("Unlocking contract in unblockContract()");
        try {
            Contracts contracts = contractRepository.findById(contractId);
            contracts.setStatus(ContractStatus.ACTIVE);
            contractRepository.update(contracts);
        } catch (ContractException e) {
            log.error("Error unblocking contract by admin", e);
        } catch (Exception e) {
            log.error("Error in AdminService.unblockContract()", e);
        }
        return "redirect:/contracts";
    }

    @Override
    @Transactional
    public String getEditContractPage(long id, Model model) {
        log.info("Rendering editContractPage in getEditContractPage()");
        contractService.renderContractPage(id, model);
        return "contract-edit-admin";
    }


    @Override
    @Transactional
    public String setTariffForContract(long contractId, long tariffId) {
        log.info("Adding tariff to contract in setTariffForContract()");
        contractService.setTariff(contractId, tariffId);
        return "redirect:/admin/edit-contract/" +contractId;
    }

    @Override
    @Transactional
    public String setOptionForContract(long contractId, long optionId) {
        log.info("Adding option to contract in setOptionForContract()");
        contractService.setOption(contractId, optionId);
        return "redirect:/admin/edit-contract/" +contractId;
    }

    @Override
    @Transactional
    public String deleteOptionsFromContract(long id, Long[] selected) {
        log.info("Deleting option from contract in deleteOptionsFromContract()");
        contractService.deleteOptionsFromContract(id, selected);
        return "redirect:/admin/edit-contract/" + id;
    }

    @Override
    @Transactional
    public String findClientByNumber(String number, Model model) {
        log.info("Finding client by id in findClientByNumber()");
        try {
            ClientsDto clientsDto = clientsMapper.toDto(clientRepository.findByNumber(number));
            model.addAttribute("client", clientsDto);
        } catch (ClientException e) {
            log.error("Error finding client by number", e);
        } catch (Exception e) {
            log.error("Error in AdminService.findClientByNumber()", e);
        }
        return "findClientByNumber";
    }

    @Override
    public List<String> getNumbers() {
        log.info("Finding client numbers in getNumbers()");
        List<String> numbers = new ArrayList<>();
        for (ContractsDto contract : contractService.findAll()) {
            numbers.add(contract.getNumber());
        }
        return numbers;
    }

    @Override
    @Transactional
    public String changeOptionStatus(long id) {
        Options option = null;
        try {
            option = optionRepository.findById(id);
            option.setActive(false);
            optionRepository.update(option);
            List<Options> options = optionRepository.findAllActive();
            for(Options curOption : options){
                if(curOption.getLinkedOptionsList().contains(option)){
                    curOption.getLinkedOptionsList().remove(option);
                }
                if(curOption.getIncompatibleOptionsList().contains(option)){
                    curOption.getIncompatibleOptionsList().remove(option);
                }
                optionRepository.update(curOption);
            }
        }catch (OptionException e) {
            log.error("Error finding option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.editOption()", e);
        }
        return "redirect:/options";
    }
}
