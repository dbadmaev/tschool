package com.dbadmaev.tschool.eCare.service.interfaces.tariffs;

import com.dbadmaev.tschool.eCare.dto.stand.TariffWithOptionsDto;
import com.dbadmaev.tschool.eCare.dto.tariff.TariffsDto;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * Tariff service interface
 *
 * @author Дмитрий
 */
public interface TariffService {

    /**
     * Find all archive tariff entities put into DTOs. Archived tariff is a tariff unable to be bought.
     *
     * @return List of tariff entities put into DTOs.
     */
    List<TariffsDto>  findAllArchive();

    /**
     * Find tariff entity by id put into DTO
     * @param id Tariff id
     * @return Tariff DTO
     */
    TariffsDto getTariff(long id);

    /**
     * Find all active tariff entities put into DTOs.
     *
     * @return List of tariff entities put into DTOs.
     */
    List<TariffsDto> getTariffsList();

    /**
     * Controller with tariff creating functionality.
     *
     * @param tariffsDto Tariff DTO
     * @param model Model receiving required data
     * @param bindingResult Binding result
     * @return redirect to tariff creating jsp
     */
    String createTariff(TariffsDto tariffsDto, Model model, BindingResult bindingResult);

    /**
     * Marking tariff as archived. Archived tariff is a tariff unable to be bought.
     * @param id
     */
    void deleteTariff(long id);

    /**
     * Getting available options to choose for current tariff.
     *
     * @param id Tariff id
     * @param model Model receiving required data
     * @return jsp with options to choose
     */
    String getAvailableOptions(long id, Model model);

    /**
     * Controller with tariff editing, choosing options for it.
     *
     * @param tariffId Tariff id
     * @param optionId Option id
     * @return redirect to tariff editing page
     */
    String addOption(long tariffId, long optionId);

    /**
     * Controller with deleting options from tariff functionality.
     *
     * @param tariffId Tariff id
     * @param optionId Option id
     * @return redirect to deleting options from tariff page
     */
    String deleteOption(long tariffId, long optionId);

    /**
     * Getting tariff and options for ad stand.
     *
     * @return List of tariff and options
     */
    List<TariffWithOptionsDto> getTariffWithOptions();
}
