package com.dbadmaev.tschool.eCare.repository.interfaces.contracts;

import com.dbadmaev.tschool.eCare.entity.Contracts;
import com.dbadmaev.tschool.eCare.exceptions.ContractException;

import java.util.List;

/**
 * Contract repository interface.
 *
 * @author Дмитрий
 */
public interface ContractRepository {

    /**
     * Find all contracts entities.
     *
     * @return List of contracts entities
     * @exception ContractException if no contracts was found
     */
    List<Contracts> findAll() throws ContractException;

    /**
     * Find contract by id.
     *
     * @param id Contracts id
     * @exception ContractException if contract wasn't found
     * @return Contracts entity
     */
    Contracts findById(long id) throws ContractException;

    /**
     * Find all client's contracts.
     *
     * @param clientId Clients id
     * @exception ContractException if no contracts found
     * @return List of all client's contracts
     */
    List<Contracts> findByClientId(long clientId) throws ContractException;

    /**
     * Find free contract.
     *
     * @exception ContractException if no contracts found
     * @return Contracts entity
     */
    Contracts findAvailableContract() throws ContractException;

    /**
     * Find last created contract.
     *
     * @exception ContractException if no contracts found
     * @return Contracts entity
     */
    Contracts findLastContract() throws ContractException;

    /**
     * Add contract entity.
     *
     * @param contract Contracts entity
     * @exception ContractException if contract wasn't saved
     */
    void save(Contracts contract) throws ContractException;

    /**
     * Update contract entity.
     *
     * @exception ContractException if contract wasn't updated
     * @param contract Contracts entity
     */
    void update(Contracts contract) throws ContractException;
}
