package com.dbadmaev.tschool.eCare.service.impl.tariffs;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.dto.stand.TariffWithOptionsDto;
import com.dbadmaev.tschool.eCare.dto.tariff.TariffsDto;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.Tariffs;
import com.dbadmaev.tschool.eCare.exceptions.OptionException;
import com.dbadmaev.tschool.eCare.exceptions.TariffException;
import com.dbadmaev.tschool.eCare.mapper.option.OptionsMapper;
import com.dbadmaev.tschool.eCare.mapper.tariff.TariffsMapper;
import com.dbadmaev.tschool.eCare.repository.interfaces.options.OptionRepository;
import com.dbadmaev.tschool.eCare.repository.interfaces.tariffs.TariffRepository;
import com.dbadmaev.tschool.eCare.service.interfaces.options.OptionsService;
import com.dbadmaev.tschool.eCare.service.interfaces.tariffs.TariffService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TariffServiceImpl implements TariffService {

    private final TariffRepository tariffRepository;
    private final TariffsMapper tariffsMapper;
    private final OptionsService optionsService;
    private final OptionsMapper optionsMapper;
    private final OptionRepository optionRepository;
    private static Logger log = LoggerFactory.getLogger(TariffServiceImpl.class);

    @Override
    public List<TariffsDto> findAllArchive() {
        log.info("Retrieving all archive tariffs");
        List<TariffsDto> tariffsDtoList = null;
        try {
            tariffsDtoList = tariffsMapper.toDtoList(tariffRepository.findAllArchive());
        } catch (TariffException e) {
            log.error("Error retrieving all archive tariffs", e);
        } catch (Exception e) {
            log.error("Error in findAllArchive()", e);
        }
        return tariffsDtoList;
    }

    @Override
    public TariffsDto getTariff(long id) {
        log.info("Finding tariff(" + id + ") in getTariff()");
        TariffsDto tariffsDto = null;
        try {
            tariffsDto = tariffsMapper.toDto(tariffRepository.findById(id));
        } catch (TariffException e) {
            log.error("Error converting tariff(" + id + ") to dto", e);
        } catch (Exception e) {
            log.error("Error in getTariff()", e);
        }
        return tariffsDto;
    }

    @Override
    public List<TariffsDto> getTariffsList() {
        log.info("Finding all tariffs in getTariffsList()");
        List<TariffsDto> tariffsDtoList = null;
        try {
            tariffsDtoList = tariffsMapper.toDtoList(tariffRepository.findAllActive());
        } catch (TariffException e) {
            log.error("Error converting tariffs to dtoList", e);
        } catch (Exception e) {
            log.error("Error in getTariffsList()", e);
        }
        return tariffsDtoList;
    }

    @Override
    @Transactional
    public String createTariff(TariffsDto tariffsDto, Model model, BindingResult bindingResult) {
        log.info("Creating tariff(" + tariffsDto.getId() + ") in createTariff()");
        if (bindingResult.hasErrors()){
            return "tariff-create";
        }
        try {
            if (tariffRepository.findByName(tariffsDto.getName()) != null) {
                model.addAttribute("nameError", "Tariff with such name is already exist");
                model.addAttribute("createTariff", new TariffsDto());
                return "tariff-create";
            }
        } catch (TariffException e) {
            log.error("Error creating tariff : " + tariffsDto.getName() + " ", e);
        } catch (Exception e) {
            log.error("Error in createTariff()", e);
        }

        Tariffs tariffs = new Tariffs();
        tariffs.setName(tariffsDto.getName());
        tariffs.setPrice(tariffsDto.getPrice());
        tariffs.setStatus(1);
        try {
            tariffRepository.save(tariffs);
        } catch (TariffException e) {
            log.error("Error saving tariff : " + tariffs.getName() + " ", e);
        } catch (Exception e) {
            log.error("Error in createTariff()", e);
        }
        return "redirect:/tariffs";
    }

    @Override
    @Transactional
    public void deleteTariff(long id) {
        log.info("Deleting tariff(" + id + ") in deleteTariff()");
        Tariffs tariffs = null;
        try {
            tariffs = tariffRepository.findById(id);
            tariffs.setStatus(0);
            tariffRepository.update(tariffs);
        } catch (TariffException e) {
            log.error("Error deleting tariff(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in deleteTariff()", e);
        }
    }

    @Override
    @Transactional
    public String getAvailableOptions(long id, Model model) {
        log.info("Getting available options for tariff(" + id + ") in getAvailableOptions()");
        List<Options> options = null;
        try {
            options = optionRepository.findAllActive();
        } catch (OptionException e) {
            log.error("Error getting available options for tariff(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in deleteTariff()", e);
        }
        Tariffs tariffs = null;
        try {
            tariffs = tariffRepository.findById(id);
            log.info("Started deleting");
            for (Options currentOptions : tariffs.getOptionsSet()){
                log.info("Deleting...");
                options.remove(currentOptions);
                deleteIncompatibleOptions(currentOptions, options);
            }
            model.addAttribute("tariffId", tariffs.getId());
            model.addAttribute("linkedOptions", tariffs.getOptionsSet());
            model.addAttribute("availableOptions", options);
            if (tariffs.getOptionsSet().size() == 0){
                model.addAttribute("availableOptions", options);
                return "tariff-options";
            }
        } catch (TariffException e) {
            log.error("Error getting available options for tariff(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in deleteTariff()", e);
        }
        return "tariff-options";
    }

    @Transactional
    public boolean checkForIncompatibleOptions(Options options) {
        log.info("Check incOpts for option with id: " + options.getId());
        return options.getIncompatibleOptionsList().size() > 0;
    }

    @Transactional
    public boolean checkForLinkedOptions(Options options) {
        log.info("Check LinkedOpts for option with id: " + options.getId());
        return options.getLinkedOptionsList().size() > 0;
    }

    @Transactional
    public void deleteIncompatibleOptions(Options options, List<Options> availableOptions) {
        log.info("Deleting IncOptions for option with id: " + options.getId());
        if(checkForIncompatibleOptions(options)) {
            for(Options currentOptions : options.getIncompatibleOptionsList()) {
                if (availableOptions.contains(currentOptions)) {
                    log.info("Options id:" + options.getId() + " , curOptions id:" + currentOptions.getId());
                    availableOptions.remove(currentOptions);
                    deleteIncompatibleOptions(currentOptions, availableOptions);
                }
            }
        }
    }

    @Override
    @Transactional
    public String addOption(long tariffId, long optionId) {
        log.info("Adding option for tariff with id: " + tariffId);
        Tariffs tariffs = null;
        try {
            tariffs = tariffRepository.findById(tariffId);
            Options options = optionsService.findById(optionId);
            tariffs.getOptionsSet().add(options);
            addAllIncompatibleOptions(tariffs, options);
        } catch (TariffException e) {
            log.error("Error adding option for tariff(" + tariffs.getId() + ")", e);
        } catch (Exception e) {
            log.error("Error in addOption()", e);
        }
        try {
            tariffRepository.update(tariffs);
        } catch (TariffException e) {
            log.error("Error updating tariff(" + tariffs.getId() + ")", e);
        } catch (Exception e) {
            log.error("Error in addOption()", e);
        }
        return "redirect:/tariffs/edit/" + tariffId;
    }

    @Transactional
    public void addAllIncompatibleOptions(Tariffs tariffs, Options options) {
        log.info("Adding incOptions for tariff with id: " + tariffs.getId());
        if (checkForLinkedOptions(options)) {
            for(Options currentOptions : options.getLinkedOptionsList()) {
                log.info("Options id:" + options.getId() + " , curOptions id:" + currentOptions.getId());
                tariffs.getOptionsSet().add(currentOptions);
                if (!tariffs.getOptionsSet().contains(currentOptions)) {
                    addAllIncompatibleOptions(tariffs, currentOptions);
                }
            }
        }
    }

    @Override
    @Transactional
    public String deleteOption(long tariffId, long optionId) {
        log.info("Deleting option from tariff with id: " + tariffId);
        Tariffs tariffs = null;
        try {
            tariffs = tariffRepository.findById(tariffId);
            Options options = optionsService.findById(optionId);
            tariffs.getOptionsSet().remove(options);
            deleteAllLinkedOptions(tariffs, options);
        } catch (TariffException e) {
            log.error("Error deleting option from tariff(" + tariffId + ")", e);
        } catch (Exception e) {
            log.error("Error in deleteOption()", e);
        }
        try {
            tariffRepository.update(tariffs);
        } catch (TariffException e) {
            log.error("Error updating tariff(" + tariffs.getId() + ")", e);
        } catch (Exception e) {
            log.error("Error in deleteOption()", e);
        }
        return "redirect:/tariffs/edit/" + tariffId;
    }


    @Transactional
    public void deleteAllLinkedOptions(Tariffs tariffs, Options options) {
        log.info("Deleting linkedOpts from tariff with id: " + tariffs.getId());
        if (checkForLinkedOptions(options)){
            for(Options currentOptions : options.getLinkedOptionsList()) {
                log.info("Options id:" + options.getId() + " , curOptions id:" + currentOptions.getId());
                if(tariffs.getOptionsSet().contains(currentOptions)) {
                    tariffs.getOptionsSet().remove(currentOptions);
                    deleteAllLinkedOptions(tariffs, currentOptions);
                }
            }
        }
    }

    @Transactional
    public List<TariffWithOptionsDto> getTariffWithOptions() {
        log.info("Retrieving all tariffs and options for ad stand");
        List<TariffWithOptionsDto> tariffWithOptionsDtos = new ArrayList<>();
        List<Tariffs> tariffs = null;
        try {
            tariffs = tariffRepository.findAllActive();
            for (Tariffs curTariff : tariffs) {
                TariffWithOptionsDto tariffWithOptionsDto = new TariffWithOptionsDto();
                TariffsDto tariffsDto = tariffsMapper.toDto(curTariff);
                List<OptionsDto> optionsDtoList = optionsMapper.toDtoList(curTariff.getOptionsSet());
                tariffsDto.setPossibleOptionsDtoList(optionsDtoList);
                tariffWithOptionsDto.setTariffsDto(tariffsDto);
                List<Options> availableOptions = optionsService.findAll();
                for (Options currentOptions : curTariff.getOptionsSet()) {
                    availableOptions.remove(currentOptions);
                    deleteIncompatibleOptions(currentOptions, availableOptions);
                }
                tariffWithOptionsDto.setOptionsDto(optionsMapper.toDtoList(availableOptions));
                tariffWithOptionsDtos.add(tariffWithOptionsDto);
            }
        } catch (TariffException e) {
            log.error("Error retrieving active tariffs", e);
        } catch (Exception e) {
            log.error("Error in getTariffWithOptions()", e);
        }
        return tariffWithOptionsDtos;
    }
}
