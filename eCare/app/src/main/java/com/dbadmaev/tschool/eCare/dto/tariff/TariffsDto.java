package com.dbadmaev.tschool.eCare.dto.tariff;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TariffsDto implements Serializable {

    private long id;

    @NotEmpty(message = "Tariff name can't be empty")
    private String name;

    private BigDecimal price;

    private int status;

    private List<OptionsDto> possibleOptionsDtoList;
}
