package com.dbadmaev.tschool.eCare.mapper.admin;

import com.dbadmaev.tschool.eCare.dto.admin.AdminsDto;
import com.dbadmaev.tschool.eCare.entity.Admins;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AdminsMapper {

    AdminsMapper ADMINS_MAPPER = Mappers.getMapper(AdminsMapper.class);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "password", target = "password")
    AdminsDto toDto(Admins admins);

    List<AdminsDto> toDtoList(List<Admins> adminsList);
}
