package com.dbadmaev.tschool.eCare.service.interfaces.clients;

import com.dbadmaev.tschool.eCare.dto.client.ClientsDto;
import com.dbadmaev.tschool.eCare.dto.client.ClientsRegistrationDto;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * Client service interface.
 *
 * @author Дмитрий
 */
public interface ClientService {

    /**
     * Find all clients entities put into DTOs.
     *
     * @return List with all clients packed into DTOs
     */
    List<ClientsDto> findAll();

    /**
     * Find client entity put into DTO by id.
     *
     * @param id Client id
     * @return Client entity put into DTO
     */
    ClientsDto findById(long id);

    /**
     * Checking client info by client registration DTO and calling save method if no errors found.
     *
     * @param clientsRegistrationDto Client registration DTO
     * @param bindingResult Binding result
     * @param model Model receiving required data
     * @return redirect to jsp with home page
     */
    String registerClient(ClientsRegistrationDto clientsRegistrationDto, BindingResult bindingResult, Model model);

    /**
     * Save client entity by client registration DTO.
     *
     * @param clientsRegistrationDto Client registration DTO
     */
    void save(ClientsRegistrationDto clientsRegistrationDto);

    /**
     * Update client entity by client DTO.
     *
     * @param clientsDto Client DTO
     */
    void update(ClientsDto clientsDto);

    /**
     * Controller with all client contracts.
     *
     * @param model Model receiving required data
     * @return jsp with required data
     */
    String getProfilePage(Model model);
}
