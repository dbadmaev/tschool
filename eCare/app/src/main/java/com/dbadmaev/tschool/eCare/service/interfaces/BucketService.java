package com.dbadmaev.tschool.eCare.service.interfaces;

import com.dbadmaev.tschool.eCare.dto.option.OptionBucketDto;
import org.springframework.ui.Model;

import java.util.ArrayList;

/**
 * Shopping bucket service interface.
 *
 * @author Дмитрий
 */
public interface BucketService {

    /**
     * Controller with bucket functionality.
     *
     * @param model Model receiving required data
     * @param bucket List of chosen options IDs and target contract put into DTO
     * @return redirect to jsp with client's bucket
     */
    String getBucketView(Model model, ArrayList<OptionBucketDto> bucket);

    /**
     * Controller with adding options to bucket to chosen contract functionality.
     *
     * @param model Model receiving required data
     * @param bucket  List of chosen options IDs and target contract put into DTO
     * @param selected Chosen options
     * @return redirect to jsp with chosen options and contracts
     */
    String addOptionsToContract(Model model, ArrayList<OptionBucketDto> bucket, Long[] selected);
}
