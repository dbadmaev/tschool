package com.dbadmaev.tschool.eCare.dto.contract;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.entity.Tariffs;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContractsDto implements Serializable {
    private long id;
    private Clients client;
    private Tariffs tariff;
    private String status;
    private String number;
    private List<OptionsDto> optionsDtoList;
}
