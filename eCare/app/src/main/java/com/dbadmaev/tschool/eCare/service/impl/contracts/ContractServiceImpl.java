package com.dbadmaev.tschool.eCare.service.impl.contracts;

import com.dbadmaev.tschool.eCare.dto.contract.ContractsDto;
import com.dbadmaev.tschool.eCare.dto.option.OptionBucketDto;
import com.dbadmaev.tschool.eCare.entity.Contracts;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.Tariffs;
import com.dbadmaev.tschool.eCare.entity.enums.ContractStatus;
import com.dbadmaev.tschool.eCare.exceptions.ClientException;
import com.dbadmaev.tschool.eCare.exceptions.ContractException;
import com.dbadmaev.tschool.eCare.exceptions.OptionException;
import com.dbadmaev.tschool.eCare.exceptions.TariffException;
import com.dbadmaev.tschool.eCare.mapper.contract.ContractsMapper;
import com.dbadmaev.tschool.eCare.repository.interfaces.clients.ClientRepository;
import com.dbadmaev.tschool.eCare.repository.interfaces.contracts.ContractRepository;
import com.dbadmaev.tschool.eCare.repository.interfaces.options.OptionRepository;
import com.dbadmaev.tschool.eCare.repository.interfaces.tariffs.TariffRepository;
import com.dbadmaev.tschool.eCare.service.interfaces.contracts.ContractService;
import com.dbadmaev.tschool.eCare.service.interfaces.options.OptionsService;
import com.dbadmaev.tschool.eCare.service.interfaces.tariffs.TariffService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContractServiceImpl implements ContractService {

    private final ContractRepository contractRepository;
    private final ClientRepository clientRepository;
    private final ContractsMapper contractsMapper;
    private final OptionsService optionsService;
    private final TariffService tariffService;
    private final TariffRepository tariffRepository;
    private final OptionRepository optionRepository;
    private static Logger log = LoggerFactory.getLogger(ContractServiceImpl.class);

    private final long DEFAULT_TARIFF = 4;

    @Override
    public List<ContractsDto> findAll() {
        log.info("Finding all contracts in findAll()");
        List<ContractsDto> contractsDtoList = null;
        try {
            contractsDtoList = contractsMapper.toDtoList(contractRepository.findAll());
        } catch (ContractException e) {
            log.error("Error finding all contracts", e);
        } catch (Exception e) {
            log.error("Error in ContractService.findAll()", e);
        }
        return contractsDtoList;
    }

    @Override
    public ContractsDto findById(long id) {
        log.info("Finding contract in findById()");
        ContractsDto contractsDto = null;
        try {
            contractsDto = contractsMapper.toDto(contractRepository.findById(id));
        } catch (ContractException e) {
            log.error("Error finding contract by id", e);
        } catch (Exception e) {
            log.error("Error in ContractService.findById()", e);
        }
        return contractsDto;
    }

    @Override
    public List<ContractsDto> findContractsByClientId(long clientId) {
        log.info("Finding client contracts in findById()");
        List<ContractsDto> contractsDtoList = null;
        try {
            contractsDtoList = contractsMapper.toDtoList(contractRepository.findByClientId(clientId));
        } catch (ContractException e) {
            log.error("Error finding client contracts", e);
        } catch (Exception e) {
            log.error("Error in ContractService.findContractsByClientId()", e);
        }
        return contractsDtoList;
    }

    @Override
    @Transactional
    public String createContract() {
        log.info("Creating new contract in createContract()");
        Contracts newContract = new Contracts();
        newContract.setStatus(ContractStatus.ACTIVE);
        newContract.setNumber(numGenerator());
        try {
            newContract.setTariff(tariffRepository.findById(DEFAULT_TARIFF));
        } catch (TariffException e) {
            log.error("Error setting tariff for current contract", e);
        } catch (Exception e) {
            log.error("Error in ContractService.createContract()", e);
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUser = authentication.getName();
        try {
            newContract.setClient(clientRepository.findByEmail(currentUser));
        } catch (ClientException e) {
            log.error("Error finding client by email", e);
        } catch (Exception e) {
            log.error("Error in ContractService.createContract()", e);
        }
        try {
            contractRepository.save(newContract);
        } catch (ContractException e) {
            log.error("Error saving contract", e);
        } catch (Exception e) {
            log.error("Error in ContractService.createContract()", e);
        }
        return "redirect:/profile";
    }

    @Override
    public String numGenerator() {
        log.info("Generating new number in numGenerator()");
        Contracts lastContract = null;
        try {
            lastContract = contractRepository.findLastContract();
        } catch (ContractException e) {
            log.error("Error finding last contract", e);
        } catch (Exception e) {
            log.error("Error in ContractService.numGenerator()", e);
        }
        StringBuilder reversedPrevNumber;
        if (lastContract == null){
            reversedPrevNumber = new StringBuilder("0000000");
        } else {
            reversedPrevNumber = new StringBuilder(lastContract.getNumber().substring(5)).reverse();
        }
        StringBuilder numPredicate = new StringBuilder("+7911");
        StringBuilder newNumber = new StringBuilder();
        int index = 0;
        while (reversedPrevNumber.charAt(index) == '9') {
            newNumber.append('0');
            index++;
        }
        newNumber.append(Character.getNumericValue(reversedPrevNumber.charAt(index)) + 1);
        index++;
        newNumber.append((reversedPrevNumber).substring(index)).reverse();
        return numPredicate.append(newNumber).toString();
    }

    @Override
    @Transactional
    public String getContractEditPage(long id, Model model, ArrayList<OptionBucketDto> bucket) {
        log.info("Rendering contract editing page in getContractEditPage()");
        renderContractPageWithBucket(id, model, bucket);
        return "contract-edit";
    }

    @Override
    @Transactional
    public void renderContractPage(long id, Model model) {
        log.info("Rendering contract page in renderContractPage()");
        Contracts contracts = null;
        try {
            contracts = contractRepository.findById(id);
        } catch (ContractException e) {
            log.error("Error finding contract by id", e);
        } catch (Exception e) {
            log.error("Error in ContractService.renderContractPage()", e);
        }
        List<Tariffs> availableTariffs = null;
        try {
            availableTariffs = tariffRepository.findAllActive();
        } catch (TariffException e) {
            log.error("Error finding all active tariffs", e);
        } catch (Exception e) {
            log.error("Error in ContractService.renderContractPage()", e);
        }
        List<Options> availableOptions = null;
        try {
            availableOptions = optionRepository.findAllActive();
        } catch (OptionException e) {
            log.error("Error finding all available options", e);
        } catch (Exception e) {
            log.error("Error in ContractService.renderContractPage()", e);
        }
        Tariffs tariffs = contracts.getTariff();
        availableTariffs.remove(tariffs);
        for (Options currentOptions : tariffs.getOptionsSet()) {
            availableOptions.remove(currentOptions);
            deleteIncompatibleOptions(currentOptions, availableOptions);
        }
        for(Options currentOptions : contracts.getContractOptionsSet()){
            availableOptions.remove(currentOptions);
            deleteIncompatibleOptions(currentOptions, availableOptions);
        }
        List<Options> copyAvailableOptions = new ArrayList<>(availableOptions);

        for(Options currentOptions : copyAvailableOptions){
            for (Options currentTariffOptions : tariffs.getOptionsSet()){
                if (currentOptions.getIncompatibleOptionsList().contains(currentTariffOptions)){
                    availableOptions.remove(currentOptions);
                    break;
                }
            }
            for (Options currentContractOptions : contracts.getContractOptionsSet()){
                if (currentOptions.getIncompatibleOptionsList().contains(currentContractOptions)){
                    availableOptions.remove(currentOptions);
                    break;
                }
            }
        }
        model.addAttribute("contract", contracts);
        model.addAttribute("tariffs", availableTariffs);
        model.addAttribute("options", availableOptions);
        model.addAttribute("curTariff", tariffs);
        model.addAttribute("curOptions", contracts.getContractOptionsSet());
    }

    @Transactional
    public void renderContractPageWithBucket(long id, Model model, ArrayList<OptionBucketDto> bucket) {
        log.info("Rendering contract page with bucket in renderContractPageWithBucket()");
        Contracts contracts = null;
        try {
            contracts = contractRepository.findById(id);
        } catch (ContractException e) {
            log.error("Error finding contract by id", e);
        } catch (Exception e) {
            log.error("Error in ContractService.renderContractPageWithBucket()", e);
        }
        List<Tariffs> availableTariffs = null;
        try {
            availableTariffs = tariffRepository.findAllActive();
        } catch (TariffException e) {
            log.error("Error finding all active tariffs", e);
        } catch (Exception e) {
            log.error("Error in ContractService.renderContractPageWithBucket()", e);
        }
        List<Options> availableOptions = null;
        try {
            availableOptions = optionRepository.findAllActive();
        } catch (OptionException e) {
            log.error("Error getting available options", e);
        } catch (Exception e) {
            log.error("Error in ContractService.renderContractPageWithBucket()", e);
        }
        Tariffs tariffs = contracts.getTariff();
        availableTariffs.remove(tariffs);
        for (Options currentOptions : tariffs.getOptionsSet()) {
            availableOptions.remove(currentOptions);
            deleteIncompatibleOptions(currentOptions, availableOptions);
        }

        for(Options currentOptions : contracts.getContractOptionsSet()) {
            availableOptions.remove(currentOptions);
            deleteIncompatibleOptions(currentOptions, availableOptions);
        }

        for(OptionBucketDto optionBucketDto : bucket){
            if (optionBucketDto.getContractId() == id){
                for(Long optionId: optionBucketDto.getOptionsIdList()){
                    availableOptions.remove(optionsService.findById(optionId));
                    deleteIncompatibleOptions(optionsService.findById(optionId), availableOptions);
                }
            }
        }

        List<Options> copyAvailableOptions = new ArrayList<>(availableOptions);

        for(Options currentOptions : copyAvailableOptions){
            for (Options currentTariffOptions : tariffs.getOptionsSet()){
                if (currentOptions.getIncompatibleOptionsList().contains(currentTariffOptions)){
                    availableOptions.remove(currentOptions);
                    break;
                }
            }
            for (Options currentContractOptions : contracts.getContractOptionsSet()){
                if (currentOptions.getIncompatibleOptionsList().contains(currentContractOptions)){
                    availableOptions.remove(currentOptions);
                    break;
                }
            }
        }

        model.addAttribute("contract", contracts);
        model.addAttribute("tariffs", availableTariffs);
        model.addAttribute("options", availableOptions);
        model.addAttribute("curTariff", tariffs);
        model.addAttribute("curOptionsNull", contracts.getContractOptionsSet().isEmpty());
        model.addAttribute("curOptions", contracts.getContractOptionsSet());
    }

    @Transactional
    public boolean checkForIncompatibleOptions(Options options) {
    log.info("Check incompatible options for option with id:" + options.getId() + "in checkForIncompatibleOptions()");
        return options.getIncompatibleOptionsList().size() > 0;
    }

    @Transactional
    public boolean checkForLinkedOptions(Options options) {
        log.info("Check linked options for option with id:" + options.getId() + "in checkForLinkedOptions()");
        return options.getLinkedOptionsList().size() > 0;
    }

    @Transactional
    public void deleteIncompatibleOptions(Options options, List<Options> availableOptions) {
        log.info("Deleting incompatible options in deleteIncompatibleOptions()");
        if(checkForIncompatibleOptions(options)) {
            for(Options currentOptions : options.getIncompatibleOptionsList()) {
                if (availableOptions.contains(currentOptions)) {
                    log.info("Options id:" + options.getId() + " , curOptions id:" + currentOptions.getId());
                    availableOptions.remove(currentOptions);
                    deleteIncompatibleOptions(currentOptions, availableOptions);
                }
            }
        }
    }

    @Override
    @Transactional
    public String setTariffForContract(long contractId, long tariffId, ArrayList<OptionBucketDto> bucket) {
        log.info("Setting tariff for contract( " + contractId + ")in setTariffForContract()");
        setTariff(contractId, tariffId);
        bucket.remove(getById(contractId, bucket));
        return "redirect:/contracts/edit/" + contractId;
    }

    public OptionBucketDto getById(long id, ArrayList<OptionBucketDto> bucket) {
        log.info("Finding bucket for contract (" + id + ") in getById()");
        for(OptionBucketDto optionBucketDto : bucket){
            if (optionBucketDto.getContractId() == id){
                return optionBucketDto;
            }
        }
        return null;
    }

    @Override
    @Transactional
    public void setTariff(long contractId, long tariffId) {
        log.info("Setting tariff for contract(" + contractId + ") in setTariff()");
        Contracts contracts = null;
        try {
            contracts = contractRepository.findById(contractId);
        } catch (ContractException e) {
            log.error("Error finding contract by id", e);
        } catch (Exception e) {
            log.error("Error in ContractService.setTariff()", e);
        }
        try {
            contracts.setTariff(tariffRepository.findById(tariffId));
            contracts.getContractOptionsSet().clear();
        } catch (TariffException e) {
            log.error("Error finding tariff by id", e);
        } catch (Exception e) {
            log.error("Error in ContractService.setTariff()", e);
        }
        try {
            contractRepository.update(contracts);
        } catch (ContractException e) {
            log.error("Error updating contract", e);
        } catch (Exception e) {
            log.error("Error in ContractService.setTariff()", e);
        }
    }

    @Override
    @Transactional
    public String addOptionToBucket(long contractId, long optionId, ArrayList<OptionBucketDto> bucket) {
        log.info("Adding option to bucket for contract(" + contractId + ") in addOptionToBucket()");
        for(OptionBucketDto optionBucketDto : bucket) {
            if (optionBucketDto.getContractId() == contractId) {
                optionBucketDto.getOptionsIdList().add(optionId);
                return "redirect:/contracts/edit/" + contractId;
            }
        }
        OptionBucketDto optionBucketDto = new OptionBucketDto();
        optionBucketDto.setContractId(contractId);
        List<Long>optionIdList = new ArrayList<>();
        optionIdList.add(optionId);
        optionBucketDto.setOptionsIdList(optionIdList);
        bucket.add(optionBucketDto);
        return "redirect:/contracts/edit/" + contractId;
    }

    @Override
    @Transactional
    public void setOption(long contractId, long optionId) {
        log.info("Adding option(" + optionId + ") to contract(" + contractId + ") in setOption()");
        Contracts contracts = null;
        Options option = null;
        try {
            contracts = contractRepository.findById(contractId);
            option = optionsService.findById(optionId);
            contracts.getContractOptionsSet().add(option);

        } catch (ContractException e) {
            log.error("Error finding contract by id", e);
        } catch (Exception e) {
            log.error("Error in ContractService.setOption()", e);
        }
        if (checkForLinkedOptions(option)) {
            for (Options curOpt : option.getLinkedOptionsList()) {
                contracts.getContractOptionsSet().add(curOpt);
            }
            try {
                contractRepository.update(contracts);
            } catch (ContractException e) {
                log.error("Error updating contract", e);
            } catch (Exception e) {
                log.error("Error in ContractService.setOption()", e);
            }
        }
    }

    @Override
    @Transactional
    public String deleteOptionsFromContract(long id, Long[] selected) {
        log.info("Deleting options from contract(" + id + ") in deleteOptionsFromContract");
        deleteOption(id, selected);
        return "redirect:/contracts/edit/" + id;
    }

    @Transactional
    public void deleteOption(long id, Long[] selected) {
        log.info("Deleting option from contract(" + id + ") in deleteOption");
        Contracts contracts = null;
        try {
            contracts = contractRepository.findById(id);
            for(long curId : selected){
                contracts.getContractOptionsSet().remove(optionsService.findById(curId));
            }
        } catch (ContractException e) {
            log.error("Error finding contract by id", e);
        } catch (Exception e) {
            log.error("Error in ContractService.deleteOption()", e);
        }
        try {
            contractRepository.update(contracts);
        } catch (ContractException e) {
            log.error("Error updating contract", e);
        } catch (Exception e) {
            log.error("Error in ContractService.deleteOption()", e);
        }
    }

    @Override
    @Transactional
    public String blockContract(long id) {
        log.info("Blocking contract in blockContract()");
        Contracts contracts = null;
        try {
            contracts = contractRepository.findById(id);
            contracts.setStatus(ContractStatus.BLOCKED_BY_USER);
        } catch (ContractException e) {
            log.error("Error blocking contract by client", e);
        } catch (Exception e) {
            log.error("Error in ContractService.blockContract()", e);
        }
        try {
            contractRepository.update(contracts);
        } catch (ContractException e) {
            log.error("Error updating contract", e);
        } catch (Exception e) {
            log.error("Error in ContractService.blockContract()", e);
        }
        return "redirect:/profile";
    }

    @Override
    @Transactional
    public String unblockContract(long id) {
        log.info("Unlocking contract in unblockContract()");
        Contracts contracts = null;
        try {
            contracts = contractRepository.findById(id);
        } catch (ContractException e) {
            log.error("Error finding contract", e);
        } catch (Exception e) {
            log.error("Error in ContractService.unblockContract()", e);
        }
        if (contracts.getStatus().equals(ContractStatus.BLOCKED_BY_USER)){
            contracts.setStatus(ContractStatus.ACTIVE);
            try {
                contractRepository.update(contracts);
            } catch (ContractException e) {
                log.error("Error unblocking contract(" + contracts.getId() + ")", e);
            } catch (Exception e) {
                log.error("Error in ContractService.unblockContract()", e);
            }
        }
        return "redirect:/profile";
    }
}
