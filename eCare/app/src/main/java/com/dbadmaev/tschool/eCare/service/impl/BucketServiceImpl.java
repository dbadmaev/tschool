package com.dbadmaev.tschool.eCare.service.impl;

import com.dbadmaev.tschool.eCare.dto.contract.ContractsDto;
import com.dbadmaev.tschool.eCare.dto.option.OptionBucketDto;
import com.dbadmaev.tschool.eCare.dto.option.OptionBucketViewDto;
import com.dbadmaev.tschool.eCare.entity.Contracts;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.exceptions.ContractException;
import com.dbadmaev.tschool.eCare.repository.interfaces.contracts.ContractRepository;
import com.dbadmaev.tschool.eCare.service.impl.contracts.ContractServiceImpl;
import com.dbadmaev.tschool.eCare.service.interfaces.BucketService;
import com.dbadmaev.tschool.eCare.service.interfaces.contracts.ContractService;
import com.dbadmaev.tschool.eCare.service.interfaces.options.OptionsService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BucketServiceImpl implements BucketService {

    private final ContractService contractService;
    private final OptionsService optionsService;
    private final ContractRepository contractRepository;
    private static Logger log = LoggerFactory.getLogger(BucketServiceImpl.class);

    @Override
    @Transactional
    public String getBucketView(Model model, ArrayList<OptionBucketDto> bucket){
        List<OptionBucketViewDto> optionBucketViewDtoList = new ArrayList<>();
        for(OptionBucketDto optionBucketDto : bucket){
            OptionBucketViewDto optionBucketViewDto = new OptionBucketViewDto();
            optionBucketViewDto.setContractId(optionBucketDto.getContractId());
            optionBucketViewDto.setNumber(contractService.findById(optionBucketDto.getContractId()).getNumber());
            List<String> optionsListName = new ArrayList<>();
            for(Long l : optionBucketDto.getOptionsIdList()){
                optionsListName.add(optionsService.findById(l).getName());
            }
            optionBucketViewDto.setOptionsListName(optionsListName);

            optionBucketViewDtoList.add(optionBucketViewDto);
        }
        model.addAttribute("listContractsNull", optionBucketViewDtoList.isEmpty());
        model.addAttribute("listContracts", optionBucketViewDtoList);
        return "bucket";
    }

    @Override
    @Transactional
    public String addOptionsToContract(Model model, ArrayList<OptionBucketDto> bucket, Long[] selected){
        for(Long contractId : selected) {
            OptionBucketDto curOptionBucketDto = getById(contractId, bucket);
            for(Long optionId : curOptionBucketDto.getOptionsIdList()) {
                contractService.setOption(contractId, optionId);
            }
            bucket.remove(curOptionBucketDto);
        }
        return "redirect:/";
    }

    public OptionBucketDto getById(long id, ArrayList<OptionBucketDto> bucket){
        for(OptionBucketDto optionBucketDto : bucket){
            if (optionBucketDto.getContractId() == id){
                return optionBucketDto;
            }
        }
        return null;
    }
}
