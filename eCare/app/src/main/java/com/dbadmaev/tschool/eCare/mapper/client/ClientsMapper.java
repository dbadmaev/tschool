package com.dbadmaev.tschool.eCare.mapper.client;

import com.dbadmaev.tschool.eCare.dto.client.ClientsDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClientsMapper {

    ClientsMapper CLIENT_MAPPER = Mappers.getMapper(ClientsMapper.class);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "blocked", target = "blocked")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "secondName", target = "secondName")
    @Mapping(source = "dateOfBirth", target = "dateOfBirth")
    @Mapping(source = "passportData", target = "passportData")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "email", target = "email")
    ClientsDto toDto(Clients clients);

    List<ClientsDto> toDtoList(List<Clients> clientsList);

    @Mapping(source = "name", target = "name")
    @Mapping(source = "secondName", target = "secondName")
    @Mapping(source = "dateOfBirth", target = "dateOfBirth")
    @Mapping(source = "passportData", target = "passportData")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "email", target = "email")
    Clients dtoToEntity(ClientsDto clientsDto);
}
