package com.dbadmaev.tschool.eCare.repository.impl.options;

import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.Options_;
import com.dbadmaev.tschool.eCare.entity.Tariffs;
import com.dbadmaev.tschool.eCare.entity.Tariffs_;
import com.dbadmaev.tschool.eCare.exceptions.OptionException;
import com.dbadmaev.tschool.eCare.repository.interfaces.options.OptionRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Transactional
public class OptionRepositoryImpl implements OptionRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Options> findAll() throws OptionException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Options> criteriaQuery = criteriaBuilder.createQuery(Options.class);
            Root<Options> root = criteriaQuery.from(Options.class);
            criteriaQuery
                    .select(root);

            TypedQuery<Options> optionsQuery = entityManager.createQuery(criteriaQuery);
            return optionsQuery.getResultList();
        } catch (PersistenceException e) {
            throw new OptionException("Error finding all options");
        }
    }

    @Override
    public Options findById(long id) throws OptionException {
        try {
            return entityManager.find(Options.class, id);
        } catch (PersistenceException e) {
            throw new OptionException("Error finding option by id");
        }
    }

    @Override
    public Options findByName(String name) throws OptionException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Options> criteriaQuery = criteriaBuilder.createQuery(Options.class);
            Root<Options> root = criteriaQuery.from(Options.class);

            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Options_.name), name));
            TypedQuery<Options> findOptionByName = entityManager.createQuery(criteriaQuery);

            return findOptionByName.getResultStream().findFirst().orElse(null);
        } catch (PersistenceException e) {
            throw new OptionException("Error finding option by name");
        }
    }

    @Override
    public List<Options> findOptionsByType(boolean isActive) throws OptionException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Options> criteriaQuery = criteriaBuilder.createQuery(Options.class);
            Root<Options> root = criteriaQuery.from(Options.class);

            criteriaQuery
                    .select(root)
                    // .orderBy(criteriaBuilder.asc(root.get(Arrays.stream(OptionType.values()).findFirst().toString())));
                    .orderBy(criteriaBuilder.asc(root.get("optionType")))
                    .where(criteriaBuilder.equal(root.get(Options_.active), isActive));

            TypedQuery<Options> findOptionsByType = entityManager.createQuery(criteriaQuery);
            return findOptionsByType.getResultStream().collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new OptionException("Error finding option by type");
        }
    }

    @Override
    public List<Options> findPossibleTariffOptions(long tariffId) throws OptionException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Options> criteriaQuery = criteriaBuilder.createQuery(Options.class);
            Root<Options> root = criteriaQuery.from(Options.class);

            Join<Options, Tariffs> tariffsOptionsJoin = root.join(Options_.tariffsList);

            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(tariffsOptionsJoin.get(Tariffs_.id), tariffId));
            TypedQuery<Options> findPossibleOptions = entityManager.createQuery(criteriaQuery);

            return findPossibleOptions.getResultStream().collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new OptionException("Error finding possible options for current tariff");
        }
    }

    @Override
    public void save(Options option) throws OptionException {
        try {
            entityManager.persist(option);
        } catch (PersistenceException e) {
            throw new OptionException("Error saving option");
        }
    }

    @Override
    public void update(Options option) throws OptionException {
        try {
            entityManager.merge(option);
        } catch (PersistenceException e) {
            throw new OptionException("Error updating option");
        }
    }

    @Override
    public List<Options> findAllActive() throws OptionException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Options> criteriaQuery = criteriaBuilder.createQuery(Options.class);
            Root<Options> root = criteriaQuery.from(Options.class);

            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Options_.active), true));
            TypedQuery<Options> selectAll = entityManager.createQuery(criteriaQuery);

            return selectAll.getResultList();
        } catch (PersistenceException e) {
            throw new OptionException("Error getting all active options");
        }
    }

    @Override
    public List<Options> findAllArchived() throws OptionException {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Options> criteriaQuery = criteriaBuilder.createQuery(Options.class);
            Root<Options> root = criteriaQuery.from(Options.class);

            criteriaQuery
                    .select(root)
                    .where(criteriaBuilder.equal(root.get(Options_.active), false));
            TypedQuery<Options> selectAll = entityManager.createQuery(criteriaQuery);

            return selectAll.getResultList();
        } catch (PersistenceException e) {
            throw new OptionException("Error getting all active options");
        }
    }
}
