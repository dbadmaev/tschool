package com.dbadmaev.tschool.eCare.service.impl.options;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.enums.OptionType;
import com.dbadmaev.tschool.eCare.exceptions.OptionException;
import com.dbadmaev.tschool.eCare.mapper.option.OptionsMapper;
import com.dbadmaev.tschool.eCare.mapper.option.OptionsToEntityMapper;
import com.dbadmaev.tschool.eCare.repository.interfaces.options.OptionRepository;
import com.dbadmaev.tschool.eCare.service.interfaces.options.OptionsService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.transaction.Transactional;
import java.util.*;

@Service
@RequiredArgsConstructor
public class OptionsServiceImpl implements OptionsService {
    //TODO add delete incomp and linked function

    private final OptionRepository optionRepository;
    private final OptionsMapper optionsMapper;
    private final OptionsToEntityMapper optionsToEntityMapper;
    private static Logger log = LoggerFactory.getLogger(OptionsServiceImpl.class);

    @Override
    public List<Options> findAll() {
        log.info("Retrieving all options in findAll method");
        List<Options> optionsList = null;
        try {
            optionsList = optionRepository.findAll();
        } catch (OptionException e) {
            log.error("Error finding all options", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.findAll()", e);
        }
        return optionsList;
    }

    @Override
    public Options findById(long id) {
        log.info("Finding option(" + id + ") in findById()");
        Options option = null;
        try {
            option = optionRepository.findById(id);
        } catch (OptionException e) {
            log.error("Error finding option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.findById()", e);
        }
        return option;
    }

    @Override
    public Options findByName(String name) {
        log.info("Finding option : " + name + " in findByName()");
        Options option = null;
        try {
            option = optionRepository.findByName(name);
        } catch (OptionException e) {
            log.error("Error finding option : " + name + " ", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.findByName()", e);
        }
        return option;
    }

    @Override
    @Transactional
    public List<OptionsDto> orderedFindAll(boolean isActive) {
        log.info("Retrieving all options ordered by type in orderedFindAll method");
        List<OptionsDto> optionsList = new ArrayList<>();
        try {
            optionsList.addAll(optionsMapper.toDtoList(optionRepository.findOptionsByType(isActive)));
        } catch (OptionException e) {
            log.error("Error finding all options", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.orderedFindAll()", e);
        }
        return optionsList;
    }

    @Override
    public List<String> getOptionTypes() {
        log.info("Retrieving option types list");
        List<String> optionTypesList = new ArrayList<>();
        for (OptionType optionTypes : OptionType.values()) {
            optionTypesList.add(optionTypes.name());
        }
        return optionTypesList;
    }

    @Override
    @Transactional
    public String editIncompatibleOptions(long id, Long[] selected, Model model) {
        log.info("Editing incompatible options to option(" + id + ") in editIncompatibleOptions method");
        Options option = null;
        try {
            option = optionRepository.findById(id);
        } catch (OptionException e) {
            log.error("Error finding option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.editIncompatibleOptions()", e);
        }

        for (Long incompatible : selected) {
            Options currentAddOption = null;
            try {
                currentAddOption = optionRepository.findById(incompatible);
                option.getIncompatibleOptionsList().add(currentAddOption);
            } catch (OptionException e) {
                log.error("Error finding option(" + incompatible + ")", e);
            } catch (Exception e) {
                log.error("Error in OptionsService.editIncompatibleOptions()", e);
            }
            if (!empty(option.getLinkedOptionsList())) {
                try {
                    if (option.getLinkedOptionsList().contains(optionRepository.findById(incompatible))) {
                        option.getLinkedOptionsList().remove(optionRepository.findById(incompatible));
                    }
                } catch (OptionException e) {
                    log.error("Error finding option(" + incompatible + ")", e);
                } catch (Exception e) {
                    log.error("Error in OptionsService.editIncompatibleOptions()", e);
                }
            }
        }
        try {
            optionRepository.update(option);
        } catch (OptionException e) {
            log.error("Error updating option(" + option.getId() + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.editIncompatibleOptions()", e);
        }
        return "redirect:/options/edit/" + id;
    }

    @Override
    @Transactional
    public String editLinkedOptions(long id, Long[] selected, Model model) {
        log.info("Editing linked options to option(" + id + ") in editLinkedOptions method");
        Options option = null;
        try {
            option = optionRepository.findById(id);
        } catch (OptionException e) {
            log.error("Error finding option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.editLinkedOptions()", e);
        }

        for (Long linked : selected) {
            Options currentAddOption = null;
            try {
                currentAddOption = optionRepository.findById(linked);
                option.getLinkedOptionsList().add(currentAddOption);
                currentAddOption.getLinkedOptionsList().add(option);
            } catch (OptionException e) {
                log.error("Error finding option(" + linked + ")", e);
            } catch (Exception e) {
                log.error("Error in OptionsService.editLinkedOptions()", e);
            }
            try {
                optionRepository.update(currentAddOption);
            } catch (OptionException e) {
                log.error("Error updating option(" + currentAddOption.getId() + ")", e);
            } catch (Exception e) {
                log.error("Error in OptionsService.editLinkedOptions()", e);
            }

            if (!empty(option.getIncompatibleOptionsList())) {
                try {
                    if (option.getIncompatibleOptionsList().contains(optionRepository.findById(linked))) {
                        option.getIncompatibleOptionsList().remove(optionRepository.findById(linked));
                    }
                } catch (OptionException e) {
                    log.error("Error removing option(" + linked + ")", e);
                } catch (Exception e) {
                    log.error("Error in OptionsService.editLinkedOptions()", e);
                }
            }
        }
        try {
            optionRepository.update(option);
        } catch (OptionException e) {
            log.error("Error removing option(" + option.getId() + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.editLinkedOptions()", e);
        }
        return "redirect:/options/edit/" + id;
    }

    @Override
    @Transactional
    public List<Options> getAvailableToAddIncompatibleOptions(long id) {
        log.info("Retrieving available to add incompatible options for option(" + id + ") in getAvailableToAddIncompatibleOptions method");
        List<Options> availableIncOptions = new ArrayList<>();
        Options option = null;
        try {
            option = optionRepository.findById(id);
        } catch (OptionException e) {
            log.error("Error finding option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.getAvailableToAddIncompatibleOptions()", e);
        }
        try {
            availableIncOptions.addAll(optionRepository.findAllActive());
        } catch (OptionException e) {
            log.error("Error retrieving availableIncOpts for option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.getAvailableToAddIncompatibleOptions()", e);
        }

        if (empty(option.getIncompatibleOptionsList())) {
            availableIncOptions.remove(option);
            return availableIncOptions;
        }
        for (Options iter : option.getIncompatibleOptionsList()) {
            if (availableIncOptions.contains(iter)) {
                availableIncOptions.remove(iter);
            }
        }
        availableIncOptions.remove(option);
        return availableIncOptions;
    }

    @Override
    @Transactional
    public List<Options> getAvailableToAddLinkedOptions(long id) {
        log.info("Retrieving available to add linked options for option(" + id + ") in getAvailableToAddLinkedOptions method");
        List<Options> availableLinkedOptions = new ArrayList<>();
        Options option = null;
        try {
            option = optionRepository.findById(id);
        } catch (OptionException e) {
            log.error("Error retrieving option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.getAvailableToAddLinkedOptions()", e);
        }
        try {
            availableLinkedOptions.addAll(optionRepository.findAllActive());
        } catch (OptionException e) {
            log.error("Error retrieving availableLinkedOpts for option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.getAvailableToAddLinkedOptions()", e);
        }

        if (empty(option.getLinkedOptionsList())) {
            availableLinkedOptions.remove(option);
            return availableLinkedOptions;
        }

        for (Options iter : option.getLinkedOptionsList()) {
            if (availableLinkedOptions.contains(iter)) {
                availableLinkedOptions.remove(iter);
            }
        }
        availableLinkedOptions.remove(option);
        return availableLinkedOptions;
    }

    @Override
    @Transactional
    public String getIncompatibleEditForm(long id, Model model) {
        log.info("Editing incompatible options for option(" + id + ") in getIncompatibleEditForm method");
        Options option = null;
        try {
            option = optionRepository.findById(id);
            boolean incompNull = false;
            boolean linkedNull = false;

            if (empty(option.getIncompatibleOptionsList())) {
                incompNull = true;
            }
            if (empty(option.getLinkedOptionsList())) {
                linkedNull = true;
            }
            model.addAttribute("linkedNull", linkedNull);
            model.addAttribute("incompNull", incompNull);
            model.addAttribute("incompatibleOptions", option.getIncompatibleOptionsList());
            model.addAttribute("linkedOptions", option.getLinkedOptionsList());
            model.addAttribute("availableOptions", getAvailableToAddIncompatibleOptions(id));

        } catch (OptionException e) {
            log.error("Error finding option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.getIncompatibleEditForm()", e);
        }
        return "incompatible-edit";
    }

    @Override
    @Transactional
    public String getLinkedEditForm(long id, Model model) {
        log.info("Editing linked options for option(" + id + ") in getLinkedEditForm method");
        Options option = null;
        try {
            option = optionRepository.findById(id);
            boolean incompNull = false;
            boolean linkedNull = false;

            if (empty(option.getIncompatibleOptionsList())) {
                incompNull = true;
            }
            if (empty(option.getLinkedOptionsList())) {
                linkedNull = true;
            }
            model.addAttribute("linkedNull", linkedNull);
            model.addAttribute("incompNull", incompNull);
            model.addAttribute("incompatibleOptions", option.getIncompatibleOptionsList());
            model.addAttribute("linkedOptions", option.getLinkedOptionsList());
            model.addAttribute("availableOptions", getAvailableToAddLinkedOptions(id));
        } catch (OptionException e) {
            log.error("Error finding option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.getLinkedEditForm()", e);
        }
        return "linked-edit";
    }

    @Override
    @Transactional
    public String editOption(long id, Model model) {
        log.info("Editing option(" + id + ") in editOption method");
        boolean incompNull = false;
        boolean linkedNull = false;
        Options option = null;
        try {
            option = optionRepository.findById(id);
            model.addAttribute("option", optionsMapper.toDto(option));
            if (empty(option.getIncompatibleOptionsList())) {
                incompNull = true;
            }
            if (empty(option.getLinkedOptionsList())) {
                linkedNull = true;
            }
            model.addAttribute("linkedNull", linkedNull);
            model.addAttribute("incompNull", incompNull);
            model.addAttribute("incompatibleOptions", option.getIncompatibleOptionsList());
            model.addAttribute("linkedOptions", option.getLinkedOptionsList());
        } catch (OptionException e) {
            log.error("Error finding option(" + id + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.editOption()", e);
        }
        return "optionEdit";
    }

    @Override
    @Transactional
    public String createOption(OptionsDto optionsDto, Model model, BindingResult bindingResult) {
        log.info("Creating new option(" + optionsDto.getId() + ") in createOption method");
        if (bindingResult.hasErrors()) {
            return "option-create";
        }
        if (findByName(optionsDto.getName()) != null) {
            model.addAttribute("optionError", "Option with such name already exists");
            model.addAttribute("createOption", new OptionsDto());
            model.addAttribute("optionTypes", getOptionTypes());
            return "option-create";
        }
        if (optionsDto.getPrice() == null) {
            model.addAttribute("priceError", "Price can't be empty, input 0 if needed");
            model.addAttribute("createOption", new OptionsDto());
            model.addAttribute("optionTypes", getOptionTypes());
            return "option-create";
        }
        if (optionsDto.getConnectionCost() == null) {
            model.addAttribute("connectionCostError", "Connection cost can't be empty, input 0 if needed");
            model.addAttribute("createOption", new OptionsDto());
            model.addAttribute("optionTypes", getOptionTypes());
            return "option-create";
        }
        save(optionsDto);
        return "redirect:/options";
    }

    @Override
    @Transactional
    public boolean empty(List<Options> optionsList) {
        log.info("Checking emptiness in empty method");
        return optionsList.size() <= 0;
    }

    @Override
    @Transactional
    public void save(OptionsDto optionsDto) {
        log.info("Persisting option(" + optionsDto.getId() + ")  in save method");
        try {
            optionRepository.save(optionsToEntityMapper.dtoToEntity(optionsDto));
        } catch (OptionException e) {
            log.error("Error saving option(" + optionsDto.getId() + ")", e);
        } catch (Exception e) {
            log.error("Error in OptionsService.save()", e);
        }
    }
}