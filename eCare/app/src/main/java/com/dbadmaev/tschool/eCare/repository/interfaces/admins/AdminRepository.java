package com.dbadmaev.tschool.eCare.repository.interfaces.admins;

import com.dbadmaev.tschool.eCare.entity.Admins;
import com.dbadmaev.tschool.eCare.exceptions.AdminException;

import java.util.List;

/**
 * Administrator repository interface.
 *
 * @author Дмитрий
 */
public interface AdminRepository {

    /**
     * Find all administrator entities.
     *
     * @exception AdminException if no entities found
     * @return list of admin entities
     */
    List<Admins> findAll() throws AdminException;

    /**
     * Find admins entity.
     *
     * @param id Admins id
     * @exception AdminException if admin isn't found
     * @return Admins entity
     */
    Admins findById(long id) throws AdminException;

    /**
     * Find admins entity by name.
     *
     * @param name Admins name
     * @exception  AdminException if admin isn't found
     * @return Admins entity
     */
    Admins findByName(String name) throws AdminException;

    /**
     * Add admins entity.
     *
     * @param admin Admins entity
     * @exception AdminException if admin wasn't saved
     */
    void save(Admins admin) throws AdminException;

    /**
     * Update admins entity.
     *
     * @param admin Admins entity
     * @exception AdminException if admin wasn't updated
     */
    void update(Admins admin) throws AdminException;
}
