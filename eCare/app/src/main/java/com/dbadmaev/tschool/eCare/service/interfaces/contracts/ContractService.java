package com.dbadmaev.tschool.eCare.service.interfaces.contracts;

import com.dbadmaev.tschool.eCare.dto.contract.ContractsDto;
import com.dbadmaev.tschool.eCare.dto.option.OptionBucketDto;
import com.dbadmaev.tschool.eCare.entity.Contracts;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Contract service interface.
 *
 * @author Дмитрий
 */
public interface ContractService {

    /**
     * Find all contract entities put into DTOs.
     *
     * @return Contract DTOs
     */
    List<ContractsDto> findAll();

    /**
     * Find contract put into DTO by id.
     *
     * @param id Contract id
     * @return Contract DTO
     */
    ContractsDto findById(long id);

    /**
     * Find all client contracts put into DTOs.
     *
     * @param clientId Client id
     * @return List of contract entities put into DTOs
     */
    List<ContractsDto> findContractsByClientId(long clientId);

    /**
     * Create contract generating number using numGenerator().
     *
     * @return redirect to jsp with all client's contracts
     */
    String createContract();

    /**
     * Phone number generation.
     *
     * @return String of phone number
     */
    String numGenerator();

    /**
     * Controller with editing contract functionality.
     *
     * @param id Contract id
     * @param model Model receiving required data
     * @param bucket List of chosen options IDs and target contract put into DTO
     * @return jsp with shopping bucket
     */
    String getContractEditPage(long id, Model model, ArrayList<OptionBucketDto> bucket);

    /**
     * Controller with setting tariff for contract functionality.
     *
     * @param contractId Contract id
     * @param tariffId Tariff id
     * @param bucket List of chosen options IDs and target contract put into DTO
     * @return redirect to jsp with contract editing
     */
    String setTariffForContract(long contractId, long tariffId, ArrayList<OptionBucketDto> bucket);

    /**
     * Controller with adding option to shopping bucket functionality.
     *
     * @param contractId Contract id
     * @param optionId Option id
     * @param bucket List of chosen options IDs and target contract put into DTO
     * @return
     */
    String addOptionToBucket(long contractId, long optionId, ArrayList<OptionBucketDto> bucket);

    /**
     * Controller with deleting options from contract functionality.
     *
     * @param id Contract id
     * @param selected Array of options IDs
     * @return redirect to jsp with contract editing functionality
     */
    String deleteOptionsFromContract(long id, Long[] selected);

    /**
     * Controller with blocking contract by client functionality.
     *
     * @param id Contract id
     * @return redirect to all client's contracts page.
     */
    String blockContract(long id);

    /**
     * Controller with unblocking contract by client functionality.
     *
     * @param id Contract id
     * @return redirect to all client's contracts page.
     */
    String unblockContract(long id);

    /**
     * Controller with info about client's contract and its tariff, tariff options, options to choose functionality.
     *
     * @param id Contract id
     * @param model Model with required data
     */
    void renderContractPage(long id, Model model);

    /**
     * Setting option for contract.
     *
     * @param contractId Contract id
     * @param optionId Option id
     */
    void setOption(long contractId, long optionId);

    /**
     * Setting tariff for contract.
     *
     * @param contractId Contract id
     * @param tariffId Tariff id
     */
    void setTariff(long contractId, long tariffId);
}
