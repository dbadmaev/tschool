package com.dbadmaev.tschool.eCare.mapper.client;

import com.dbadmaev.tschool.eCare.dto.client.ClientsRegistrationDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ClientsRegistrationMapper {
    ClientsRegistrationMapper CLIENTS_REGISTRATION_MAPPER = Mappers.getMapper(ClientsRegistrationMapper.class);

    @Mapping(source = "role", target = "role")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "secondName", target = "secondName")
    @Mapping(source = "dateOfBirth", target = "dateOfBirth")
    @Mapping(source = "passportData", target = "passportData")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "password", target = "password")
    ClientsRegistrationDto toDto(Clients clients);

    @Mapping(source = "role", target = "role")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "secondName", target = "secondName")
    @Mapping(source = "dateOfBirth", target = "dateOfBirth")
    @Mapping(source = "passportData", target = "passportData")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "password", target = "password")
    Clients dtoToEntity(ClientsRegistrationDto clientsRegistrationDto);
}
