package com.dbadmaev.tschool.eCare.service.impl;

import com.dbadmaev.tschool.eCare.entity.Admins;
import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.exceptions.AdminException;
import com.dbadmaev.tschool.eCare.exceptions.ClientException;
import com.dbadmaev.tschool.eCare.repository.interfaces.admins.AdminRepository;
import com.dbadmaev.tschool.eCare.repository.interfaces.clients.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final ClientRepository clientRepository;
    private final AdminRepository adminRepository;
    private static Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        log.info("Signing in" + login);
        Admins admin = null;
        try {
            admin = adminRepository.findByName(login);
        } catch (AdminException e) {
            log.error("Error finding admin with login : " + login, e);
        } catch (Exception e) {
            log.error("Error in loadUserByUsername()", e);
        }

        if (admin != null) {
            return new User(
                    admin.getName(), admin.getPassword(),
                    Collections.singleton(new SimpleGrantedAuthority(admin.getRole())));
        }
        Clients client = null;
        try {
            client = clientRepository.findByEmail(login);
        } catch (ClientException e) {
            log.error("Error finding user with login : " + login, e);
        } catch (Exception e) {
            log.error("Error in loadUserByUsername()", e);
        }
        if (client == null || client.getBlocked() == 1) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new User(
                client.getEmail(), client.getPassword(),
                Collections.singleton(new SimpleGrantedAuthority(client.getRole())));
    }
}
