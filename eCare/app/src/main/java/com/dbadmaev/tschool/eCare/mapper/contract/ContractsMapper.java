package com.dbadmaev.tschool.eCare.mapper.contract;

import com.dbadmaev.tschool.eCare.dto.contract.ContractsDto;
import com.dbadmaev.tschool.eCare.entity.Contracts;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class ContractsMapper {

    private static Logger log = LoggerFactory.getLogger(ContractsMapper.class);

    public ContractsDto toDto (Contracts contract) {
        log.info("Converting contract to dto");

        ContractsDto contractsDto = new ContractsDto();
        contractsDto.setId(contract.getId());
        contractsDto.setStatus(contract.getStatus().toString());
        contractsDto.setNumber(contract.getNumber());
        contractsDto.setClient(contract.getClient());
        contractsDto.setTariff(contract.getTariff());

        return contractsDto;
    }

    public List<ContractsDto> toDtoList(List<Contracts> contractsList) {
        log.info("Converting contractList to dtoList");
        List<ContractsDto> contractsDtoList = new ArrayList<>();

        for (Contracts contract : contractsList) {
            contractsDtoList.add(toDto(contract));
        }
        return contractsDtoList;
    }
}
