package com.dbadmaev.tschool.eCare.controller;

import com.dbadmaev.tschool.eCare.dto.stand.TariffWithOptionsDto;
import com.dbadmaev.tschool.eCare.service.interfaces.tariffs.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class StandController {

    private final TariffService tariffService;

    @GetMapping("/stand")
    public List<TariffWithOptionsDto> getTariffs(){
        return tariffService.getTariffWithOptions();
    }
}
