<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main page</title>
    <link rel="stylesheet" type="text/css" href="/css/home-page.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
    <div class="main">
        <div class="slogan">
            <p1>Stay in touch</p1>
        </div>
        <div class="description">
            <p2>Get a phone number, choose tariff and add options to it!</p2>
        </div>
        <c:if test="${pageContext.request.userPrincipal.name == null}">
            <a class="button" href="${contextPath}/registration">connect</a>
        </c:if>
    </div>
        <img src="/images/home-pic.svg" class="pic">
</div>
</body>
</html>
