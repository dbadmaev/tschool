<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Contracts</title>
    <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
    <div class="table-custom">
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Number</th>
                    <th>Tariff</th>
                    <th>Client</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${contracts}" var="contract">
                <tr>
                    <td><c:out value="${contract.id}"/> </td>
                    <td><c:out value="${contract.number}"/> </td>
                    <td><c:out value="${contract.tariff.getName()}"/> </td>
                    <td><c:out value="${contract.client.getName()}"/></td>
                    <td><c:out value="${contract.status}"/> </td>
                    <td><a href="${pageContext.request.contextPath}/admin/edit-contract/${contract.id}">Edit</a></td>

                    <c:if test="${contract.status.equals('BLOCKED_BY_ADMIN')}">
                        <td><a class="status status-unblock"
                                href="${pageContext.request.contextPath}/admin/unblock-contract/${contract.id}">Unblock</a></td>
                    </c:if>

                    <c:if test="${contract.status.equals('BLOCKED_BY_USER')}">
                        <td><a class="status status-unblock"
                                href="${pageContext.request.contextPath}/admin/unblock-contract/${contract.id}">Unblock</a></td>
                    </c:if>

                    <c:if test="${contract.status.equals('ACTIVE')}">
                        <td><a class="status status-block" href="/admin/block-contract/${contract.id}">Block</a></td>
                    </c:if>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
