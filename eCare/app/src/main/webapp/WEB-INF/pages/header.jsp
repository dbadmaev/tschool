<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/header.css">
</head>
<body>
<header>
    <div class="container">
        <a href="${contextPath}/home"><img src="/images/LOGO.svg" class="logo" alt="eCare."></a>
        <nav>
            <ul class="menu">
                <c:if test="${pageContext.request.userPrincipal.name == null}">
                <li>
                    <a class="button-big" href="${contextPath}/login">Log in</a>
                </li>
                </c:if>
                <c:if test="${pageContext.request.userPrincipal.name != null}">
                <li>
                    <form id="logoutForm" method="post" action="${contextPath}/logout">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                </li>
                <security:authorize access="hasAuthority('ADMIN')">
                <li>
                    <a class="with-hover" href="${contextPath}/admin">Find client</a>
                </li>
                <li>
                    <a class="with-hover" href="${pageContext.request.contextPath}/admin/allClients">Clients</a>
                </li>
                <li>
                    <a class="with-hover" href="${contextPath}/options">Options</a>
                </li>
                <li>
                    <a class="with-hover" href="${contextPath}/tariffs">Tariffs</a>
                </li>
                <li>
                    <a class="with-hover" href="${contextPath}/contracts">Contracts</a>
                </li>
                <li>
                    <a class="with-hover" href="${contextPath}/admin/archive">Archive</a>
                </li>
                </security:authorize>

                <security:authorize access="hasAuthority('USER')">
                <li>
                    <a class="with-hover" href="${contextPath}/profile">Profile</a>
                </li>
                <li>
                    <a href="${contextPath}/bucket"> <img src="/images/trolley.svg" class="shopping-cart"> </a>
                </li>

                </security:authorize>
                <li>
                    <a class="button-big" onclick="document.forms['logoutForm'].submit()">Logout</a>
                </li>
                </c:if>
            </ul>
        </nav>
    </div>
</header>
</body>
</html>