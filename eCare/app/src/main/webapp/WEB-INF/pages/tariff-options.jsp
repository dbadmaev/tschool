<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Available options</title>
    <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<form:form method="post" action="/tariffs/delete/${tariffId}">
<div class="container">
    <div class="table-custom">
        <h2>Possible options</h2>
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Name</th>
                <th>Price</th>
                <th>Connection cost</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${linkedOptions}" var="options">
                <tr>
                    <td><c:out value="${options.id}"/></td>
                    <td><c:out value="${options.optionType}"/> </td>
                    <td><c:out value="${options.name}"/> </td>
                    <td><c:out value="${options.price}"/> </td>
                    <td><c:out value="${options.connectionCost}"/> </td>
                    <td><input type="checkbox" name="selected" value="${options.id}" /></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <input type="submit" value="Delete" />
    </div>
</div>
</form:form>

<form:form method="post">
<div class="container">
    <div class="table-custom">
        <h2>Choose option to add</h2>
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Name</th>
                <th>Price</th>
                <th>Connection cost</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${availableOptions}" var="options">
                <tr>
                    <td><c:out value="${options.id}"/></td>
                    <td><c:out value="${options.optionType}"/> </td>
                    <td><c:out value="${options.name}"/> </td>
                    <td><c:out value="${options.price}"/> </td>
                    <td><c:out value="${options.connectionCost}"/> </td>
                    <td><input type="checkbox" name="selected" value="${options.id}" /></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <input type="submit" value="Add" />
    </div>
</div>
</form:form>
</body>
</html>
