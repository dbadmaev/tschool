<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit contract</title>
    <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
    <h2>${contract.getNumber()}</h2>
    <div class="table-custom">
        <h2>Current tariff</h2>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><c:out value="${curTariff.getName()}"/> </td>
                <td><c:out value="${curTariff.getPrice()}"/> </td>
                <c:if test="${curTariff.getStatus() == 1}">
                    <td>Active</td>
                </c:if>
                <c:if test="${curTariff.getStatus() == 0}">
                    <td>Archive</td>
                </c:if>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="container">
    <form:form method="post" action="/contracts/delete-options/${contract.getId()}">
        <div class="table-custom">
            <h2>Tariff options</h2>
            <table>
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Connection cost</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${curTariff.getOptionsSet()}" var="option">
                    <tr>
                        <td><c:out value="${option.getOptionType()}"/> </td>
                        <td><c:out value="${option.getName()}"/> </td>
                        <td><c:out value="${option.getPrice()}"/> </td>
                        <td><c:out value="${option.getConnectionCost()}"/> </td>
                        <c:if test="${option.isActive()}">
                            <td>Active</td>
                        </c:if>
                        <c:if test="${!option.isActive()}">
                            <td>Archive</td>
                        </c:if>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </form:form>
</div>

<div class="container">
    <form:form method="post" action="/contracts/delete-options/${contract.getId()}">
        <div class="table-custom">
            <h2>Current options</h2>
            <table>
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Connection cost</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${!curOptionsNull}">
                    <c:forEach items="${curOptions}" var="option">
                        <tr>
                            <td><c:out value="${option.getOptionType()}"/> </td>
                            <td><c:out value="${option.getName()}"/> </td>
                            <td><c:out value="${option.getPrice()}"/> </td>
                            <td><c:out value="${option.getConnectionCost()}"/> </td>
                            <td><input type="checkbox" name="selected" value="${option.getId()}" /></td>
                        </tr>
                    </c:forEach>
                </c:if>
                </tbody>
            </table>
            <input type="submit" value="Delete" />
        </div>
    </form:form>
</div>

<div class="container">
    <form:form method="post" action="/contracts/edit-tariff/${contract.getId()}">
        <div class="table-custom">
            <h2>Change Tariff</h2>
            <table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${tariffs}" var="tariff">
                    <tr>
                        <td><c:out value="${tariff.getName()}"/> </td>
                        <td><c:out value="${tariff.getPrice()}"/> </td>
                        <td><input type="checkbox" name="selected" value="${tariff.getId()}" /></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <input type="submit" value="Select" />
        </div>
    </form:form>
</div>

<div class="container">
    <form:form method="post" action="/contracts/edit-options/${contract.getId()}">
        <div class="table-custom">
            <h2>Choose Option</h2>
            <table>
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Connection cost</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${options}" var="option">
                    <tr>
                        <td><c:out value="${option.getOptionType()}"/> </td>
                        <td><c:out value="${option.getName()}"/> </td>
                        <td><c:out value="${option.getPrice()}"/> </td>
                        <td><c:out value="${option.getConnectionCost()}"/> </td>
                        <td><input type="checkbox" name="selected" value="${option.getId()}" />
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <input type="submit" value="CONFIRM" />
        </div>
    </form:form>
</div>

</body>
</html>
