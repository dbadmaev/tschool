<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Find client</title>
</head>
<body>
<h1>Client information</h1>
<form action="/client/find/${client.id}">
    <table class="clients table-bordered">
        <tr>
            <th>First name:</th>
            <th>Second Name</th>
            <th>Date of birth</th>
            <th>Passport data</th>
            <th>Address</th>
            <th>Email</th>
        </tr>
        <tr>
            <td>"${client.name}"</td>
            <td>"${client.secondName}"</td>
            <td>"${client.dateOfBirth}"</td>
            <td>"${client.passportData}"</td>
            <td>"${client.address}"</td>
            <td>"${client.email}"</td>
        </tr>
    </table>
</form>
</body>
</html>
