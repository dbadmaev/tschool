<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Administrator</title>
    <link rel="stylesheet" href="/css/adminPage.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
    <form:form method="post" action="/admin/findByNumber" cssClass="client-search">
        <input class="search-form" type="text" name="number" placeholder="Number">
        <input class="search-button" type="submit" value="Search">
    </form:form>
</div>
</body>
</html>
