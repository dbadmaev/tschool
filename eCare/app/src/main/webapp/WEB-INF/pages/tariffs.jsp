<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tariffs</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<%--TODO add table name--%>
<%--TODO refactor create button--%>
<%--TODO center table vertically?--%>
<div class="container">
    <div class="table-custom">
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th><a class="button-create" href="/tariffs/create/">create tariff</a></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${tariffs}" var="tariff">
                <tr>
                    <td><c:out value="${tariff.id}"/></td>
                    <td><c:out value="${tariff.name}"/> </td>
                    <td><c:out value="${tariff.price}"/> </td>
                    <td><a href="/tariffs/edit/${tariff.id}"><img src="/images/edit.svg" class="edit"> </a></td>
                    <td><a href="/tariffs/delete/${tariff.id}"><img src="/images/delete.svg" class="delete"></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
<%--        <a class="under-table" href="/tariffs/create/">create tariff</a>--%>
    </div>
</div>
</body>
</html>
