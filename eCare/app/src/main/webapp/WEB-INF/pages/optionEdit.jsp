<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit option</title>
    <link rel="stylesheet" type="text/css" href="/css/option-edit.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
    <div class="row">
        <div class="column">
            <h2>Incompatible options</h2>
            <table>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Connection cost</th>
                </tr>
        <c:if test = "${incompNull == false}">
            <c:forEach items="${incompatibleOptions}" var="incOpts">
                <tr>
                    <td><c:out value="${incOpts.id}"/></td>
                    <td><c:out value="${incOpts.optionType}"/> </td>
                    <td><c:out value="${incOpts.name}"/> </td>
                    <td><c:out value="${incOpts.price}"/> </td>
                    <td><c:out value="${incOpts.connectionCost}"/> </td>
                </tr>
            </c:forEach>
        </c:if>
            </table>
            <a class="under-table" href="/options/edit-incompatible/${option.id}">Edit incompatible options</a>
        </div>

        <div class="column2">
            <h2>Linked options</h2>
            <table>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Connection cost</th>
                </tr>
        <c:if test= "${linkedNull == false}">
            <c:forEach items="${linkedOptions}" var="linkOpts">
                <tr>
                    <td><c:out value="${linkOpts.id}"/></td>
                    <td><c:out value="${linkOpts.optionType}"/> </td>
                    <td><c:out value="${linkOpts.name}"/> </td>
                    <td><c:out value="${linkOpts.price}"/> </td>
                    <td><c:out value="${linkOpts.connectionCost}"/> </td>
                </tr>
            </c:forEach>
        </c:if>
            </table>
            <a class="under-table" href="/options/edit-linked/${option.id}">Edit linked options</a>
        </div>
    </div>
</div>
</body>
</html>