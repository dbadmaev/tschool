<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit incompatible options</title>
</head>
<body>
<%@include file="header.jsp"%>

<h3>Incompatible options</h3>

    <table>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Type</th>
                <th scope="col">Name</th>
                <th scope="col">Price</th>
                <th scope="col">Connection cost</th>
            </tr>
            </thead>
            <tbody>

            <c:if test = "${incompNull == false}">
                <c:forEach items="${incompatibleOptions}" var="incOpts">
                    <tr>
                        <td><c:out value="${incOpts.id}"/></td>
                        <td><c:out value="${incOpts.optionType}"/> </td>
                        <td><c:out value="${incOpts.name}"/> </td>
                        <td><c:out value="${incOpts.price}"/> </td>
                        <td><c:out value="${incOpts.connectionCost}"/> </td>
                    </tr>
                </c:forEach>
            </c:if>

            </tbody>
        </table>
    </table>


    <h3>Linked options</h3>

        <table>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Type</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Connection cost</th>
                </tr>
                </thead>
                <tbody>
        <c:if test= "${linkedNull == false}">

                <c:forEach items="${linkedOptions}" var="linkOpts">
                    <tr>
                        <td><c:out value="${linkOpts.id}"/></td>
                        <td><c:out value="${linkOpts.optionType}"/> </td>
                        <td><c:out value="${linkOpts.name}"/> </td>
                        <td><c:out value="${linkOpts.price}"/> </td>
                        <td><c:out value="${linkOpts.connectionCost}"/> </td>
                    </tr>
                </c:forEach>
        </c:if>
                </tbody>
            </table>
        </table>

<a href="/options/edit-linked/${id}">Edit linked options</a>

    <h3>Available options</h3>



        <form:form method="post" >
            <table>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Type</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Connection cost</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test= "${availableOptions.size() != 0}">
                    <c:forEach items="${availableOptions}" var="availOpts">
                        <tr>
                            <td><c:out value="${availOpts.id}"/></td>
                            <td><c:out value="${availOpts.optionType}"/> </td>
                            <td><c:out value="${availOpts.name}"/> </td>
                            <td><c:out value="${availOpts.price}"/> </td>
                            <td><c:out value="${availOpts.connectionCost}"/> </td>
                            <td><input type="checkbox" name="selected" value="${availOpts.id}" /></td>
                        </tr>
                    </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </table>
            <input type="submit" value="Add" />
        </form:form>

</body>
</html>
