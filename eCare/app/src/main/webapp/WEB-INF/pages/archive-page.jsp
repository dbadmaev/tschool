<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Archive</title>
  <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
  <div class="table-custom">
    <h2>Options</h2>
    <table>
      <thead>
      <tr>
        <th>ID</th>
        <th>Type</th>
        <th>Name</th>
        <th>Price</th>
        <th>Connection cost</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${options}" var="options">
        <tr>
          <td><c:out value="${options.id}"/></td>
          <td><c:out value="${options.optionType}"/> </td>
          <td><c:out value="${options.name}"/> </td>
          <td><c:out value="${options.price}"/> </td>
          <td><c:out value="${options.connectionCost}"/> </td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>
  <div class="table-custom">
    <h2>Tariffs</h2>
    <table>
      <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Price</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${tariffs}" var="tariffs">
        <tr>
          <td><c:out value="${tariffs.id}"/></td>
          <td><c:out value="${tariffs.name}"/> </td>
          <td><c:out value="${tariffs.price}"/> </td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>
</div>
</body>
</html>
