<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Options</title>
    <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
    <div class="table-custom">
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Connection cost</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${options}" var="options">
                <tr>
                    <td><c:out value="${options.id}"/></td>
                    <td><c:out value="${options.optionType}"/> </td>
                    <td><c:out value="${options.name}"/> </td>
                    <td><c:out value="${options.price}"/> </td>
                    <td><c:out value="${options.connectionCost}"/> </td>
                    <td><a href="/options/edit/${options.id}">Edit</a></td>
                    <td><a href="/admin/change-status/${options.id}">Delete</a></td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
        <a class="under-table" href="/options/create">create option</a>
    </div>
</div>
</body>
</html>
