<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>Create option</title>
</head>
<body>
<%@include file="header.jsp"%>
<form:form method='POST' modelAttribute="createTariff">
<div class="container">
    <spring:bind path="name">
        <dd>
            <form:input type="text" path="name" id="name" class="vvod"
                        placeholder="name"/>
            <form:errors path="name" />
            <c:if test="${nameError!=null}">
                <div>
                    <span>${nameError}</span>
                </div>
            </c:if>
        </dd>
    </spring:bind>

    <spring:bind path="price">
        <dd >
            <form:input type="BigDecimal"  path="price" id="price" class="vvod"
                        placeholder="price"/>
            <form:errors path="price" />
            <c:if test="${priceError!=null}">
                <div>
                    <span>${priceError}</span>
                </div>
            </c:if>
        </dd>
    </spring:bind>

    <input type='submit' value='Create'>


</form:form>
</div>
</body>
</html>
