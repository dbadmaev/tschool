<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Clients</title>
    <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
    <div class="table-custom">
        <table>
            <thead>
            <tr>
                <th>First name</th>
                <th>Last Name</th>
                <th>Date of birth</th>
                <th>Passport data</th>
                <th>Address</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${clients}" var="client">
            <tr>
                <td><c:out value="${client.name}"/></td>
                <td><c:out value="${client.secondName}"/></td>
                <td><c:out value="${client.dateOfBirth}"/></td>
                <td><c:out value="${client.passportData}"/></td>
                <td><c:out value="${client.address}"/></td>
                <td><c:out value="${client.email}"/></td>

                <c:if test="${client.blocked == 1}">
                    <td><a class="status status-unblock"
                           href="${pageContext.request.contextPath}/admin/unblock-user/${client.getId()}">Unblock</a></td>
                </c:if>

                <c:if test="${client.blocked == 0}">
                    <td><a class="status status-block" href="/admin/block-user/${client.id}"> Block</a></td>
                </c:if>
            </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>