<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Bucket</title>
    <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
<c:if test="${listContractsNull}">
    <h2>Nothing added</h2>
</c:if>

<c:if test="${!listContractsNull}">
    <form:form method="post" action="/bucket/buy">
        <c:forEach items="${listContracts}" var="contract">
            <h2>${contract.getNumber()}</h2>
            <div class="table-custom">
                <table>
                    <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${contract.getOptionsListName()}" var="option">
                        <tr>
                            <td><c:out value="${option}"/> </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <input type="checkbox" name="selected" value="${contract.getContractId()}" />
                <input type="submit" value="Buy" />
            </div>
        </c:forEach>

    </form:form>
</c:if>
</div>
</body>
</html>
