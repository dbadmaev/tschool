<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Registration page</title>
</head>
<body>
<%@include file="header.jsp"%>
<form:form method='POST' modelAttribute="client">
<div class="container">
    <spring:bind path="name">
        <dd >
            <form:input type="text"  path="name" id="name" class="vvod"
                        placeholder="Name"/>
            <form:errors path="name" />
                <div>
                    <span></span>
                </div>
        </dd>
    </spring:bind>

    <spring:bind path="secondName">
        <dd>
            <form:input type="text" path="secondName" id="secondName" class="vvod"
                        placeholder="Second name"/>
            <form:errors path="secondName" />
            <div>
                <span></span>
            </div>
        </dd>
    </spring:bind>

    <spring:bind path="dateOfBirth">
        <dd >
            <form:input type="date"  path="dateOfBirth" id="dateOfBirth" class="vvod"
                        placeholder="Date of birth"/>
            <form:errors path="dateOfBirth" />
            <div>
                <span></span>
            </div>
        </dd>
    </spring:bind>

    <spring:bind path="passportData">
        <dd>
            <form:input type="text" path="passportData" id="passportData" class="vvod"
                        placeholder="Passport data"/>
            <form:errors path="passportData" />
            <div>
                <span></span>
            </div>
        </dd>
    </spring:bind>

    <spring:bind path="address">
        <dd >
            <form:input type="text"  path="address" id="address" class="vvod"
                        placeholder="Address"/>
            <form:errors path="address" />
            <div>
                <span></span>
            </div>
        </dd>
    </spring:bind>

    <spring:bind path="email">
        <dd>
            <form:input type="email" path="email" id="email" class="vvod"
                        placeholder="Email"/>
            <form:errors path="email" />
            <c:if test="${userError!=null}">
                <div>
                    <span>${userError}</span>
                </div>
            </c:if>
        </dd>
    </spring:bind>

    <spring:bind path="confirmEmail">
        <dd >
            <form:input type="email"  path="confirmEmail" id="confirmEmail" class="vvod"
                        placeholder="Confirm your email"/>
            <form:errors path="confirmEmail" />
            <div>
                <span></span>
            </div>
            <c:if test="${emailError!=null}">
                <div>
                    <span>${emailError}</span>
                </div>
            </c:if>
        </dd>
    </spring:bind>

    <spring:bind path="password">
        <dd>
            <form:input type="password" path="password" id="password" class="vvod"
                        placeholder="Password"/>
            <form:errors path="password" />
            <div>
                <span></span>
            </div>
        </dd>
    </spring:bind>

    <spring:bind path="confirmPassword">
        <dd >
            <form:input type="password"  path="confirmPassword" id="confirmPassword" class="vvod"
                        placeholder="Confirm your password"/>
            <form:errors path="confirmPassword" />
            <c:if test="${passwordError!=null}">
                <div>
                    <span>${passwordError}</span>
                </div>
            </c:if>
        </dd>
    </spring:bind>


    <input type='submit' value='Register'>

</form:form>
</div>
</body>
</html>