<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Contracts</title>
    <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
    <h2>${contract.getNumber()}</h2>
    <div class="table-custom">
        <h2>Your contracts</h2>
        <table>
            <thead>
                <tr>
                    <th>Number</th>
                    <th>Tariff</th>
                </tr>
            </thead>
            <tbody>
            <c:if test= "${!contractsNull}">
                <c:forEach items="${contracts}" var="contract">
                    <tr>
                        <td><c:out value="${contract.getNumber()}"/> </td>
                        <td><c:out value="${contract.getTariff().getName()}"/> </td>
                        <c:if test= "${contract.getStatus().equals('ACTIVE')}">
                            <td><a href="${pageContext.request.contextPath}/contracts/edit/${contract.getId()}">Edit</a></td>
                        </c:if>

                        <c:if test= "${contract.getStatus().equals('BLOCKED_BY_USER')}">
                            <td><a href="${pageContext.request.contextPath}/contracts/unblock/${contract.getId()}">Unblock</a></td>
                        </c:if>

                        <c:if test= "${contract.getStatus().equals('ACTIVE')}">
                            <td><a href="${pageContext.request.contextPath}/contracts/block/${contract.getId()}">Block</a></td>
                        </c:if>
                        <c:if test= "${contract.getStatus().equals('BLOCKED_BY_ADMIN')}">
                            <td><a>Blocked by administrator</a></td>
                        </c:if>
                    </tr>
                </c:forEach>
            </c:if>
            </tbody>
        </table>
        <a class="under-table" href="/profile/create">Get new number</a>
    </div>
</div>

</body>
</html>
