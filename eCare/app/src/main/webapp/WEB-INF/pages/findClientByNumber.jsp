<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Find client</title>
    <link rel="stylesheet" type="text/css" href="/css/table.css">
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
    <div class="table-custom">
        <table>
            <thead>
                <tr>
                    <th>First name</th>
                    <th>Second Name</th>
                    <th>Date of birth</th>
                    <th>Passport data</th>
                    <th>Address</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>${client.name}</td>
                    <td>${client.secondName}</td>
                    <td>${client.dateOfBirth}</td>
                    <td>${client.passportData}</td>
                    <td>${client.address}</td>
                    <td>${client.email}</td>
                    <c:if test="${client.blocked == 1}">
                        <td><a class="status status-unblock"
                               href="${pageContext.request.contextPath}/admin/unblock-user/${client.getId()}">Unblock</a></td>
                    </c:if>

                    <c:if test="${client.blocked == 0}">
                        <td><a class="status status-block"
                               href="/admin/block-user/${client.id}"> Block</a></td>
                    </c:if>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
