package com.dbadmaev.service;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.enums.OptionType;
import com.dbadmaev.tschool.eCare.exceptions.OptionException;
import com.dbadmaev.tschool.eCare.mapper.option.OptionsMapper;
import com.dbadmaev.tschool.eCare.mapper.option.OptionsToEntityMapper;
import com.dbadmaev.tschool.eCare.repository.interfaces.options.OptionRepository;
import com.dbadmaev.tschool.eCare.service.impl.options.OptionsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OptionsServiceTest {
    @Mock
    private OptionRepository optionRepository;

    @Mock
    private OptionsMapper optionsMapper;

    @Mock
    private OptionsToEntityMapper optionsToEntityMapper;

    @InjectMocks
    private OptionsServiceImpl optionsService;

    private List<Options> getIncompatibleOptions() {
        List<Options> incList = new ArrayList<>();
        incList.add(Options.builder()
                .id(2)
                .version(1)
                .active(true)
                .optionType(OptionType.CALLS)
                .name("testInc")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .build());
        return incList;
    }

    private List<Options> getLinkedOptions() {
        List<Options> linkedOpts = new ArrayList<>();
        linkedOpts.add(Options.builder()
                .id(3)
                .version(1)
                .active(true)
                .optionType(OptionType.INTERNET)
                .name("testLinked")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .build());
        return linkedOpts;
    }

    private Options getOptionsEntity() {
        return Options.builder()
                .id(1)
                .version(1)
                .active(true)
                .optionType(OptionType.INTERNET)
                .name("test")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .incompatibleOptionsList(getIncompatibleOptions())
                .linkedOptionsList(getLinkedOptions())
                .build();
    }

    private OptionsDto getOptionsDto() {
        return OptionsDto.builder()
                .id(1)
                .optionType("INTERNET")
                .name("test")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .build();
    }

    private List<Options> getAllTestOptions() {
        List<Options> optionsList = new ArrayList<>();
        optionsList.add(getIncompatibleOptions().get(0));
        optionsList.add(getLinkedOptions().get(0));
        optionsList.add(getOptionsEntity());
        return optionsList;
    }

    @Test
    public void findAllTest() throws OptionException {
        when(optionRepository.findAll()).thenReturn(Collections.singletonList(getOptionsEntity()));
        List<Options> test = optionRepository.findAll();
        assertEquals(test.get(0), getOptionsEntity());
    }

    @Test
    public void findByIdTest() throws OptionException {
        when(optionRepository.findById(anyLong())).thenReturn(getOptionsEntity());
        Options test = optionRepository.findById(1);
        assertEquals(test, getOptionsEntity());
    }

    @Test
    public void findByNameTest() throws OptionException {
        when(optionRepository.findByName(anyString())).thenReturn(getOptionsEntity());
        Options test = optionRepository.findByName("test");
        assertEquals(test, getOptionsEntity());
    }

    @Test
    public void orderedFindAllTest() throws OptionException {
        when(optionRepository.findOptionsByType(anyBoolean())).thenReturn(Collections.singletonList(getOptionsEntity()));
        when(optionsMapper.toDtoList(anyList())).thenReturn(Collections.singletonList(getOptionsDto()));
        List<OptionsDto> test = optionsService.orderedFindAll(anyBoolean());
        assertEquals(test.get(0), getOptionsDto());
    }

    @Test
    public void editIncompatibleOptionsTest() throws OptionException {
        lenient().when(optionRepository.findById(getIncompatibleOptions().get(0).getId())).thenReturn(getIncompatibleOptions().get(0));
        lenient().when(optionRepository.findById(anyLong())).thenReturn(getOptionsEntity());
        optionRepository.update(getOptionsEntity());
        verify(optionRepository, only()).update(any(Options.class));
    }

    @Test
    public void editLinkedOptionsTest() throws OptionException {
        lenient().when(optionRepository.findById(getLinkedOptions().get(0).getId())).thenReturn(getLinkedOptions().get(0));
        lenient().when(optionRepository.findById(anyLong())).thenReturn(getOptionsEntity());
        optionRepository.update(getOptionsEntity());
        verify(optionRepository, only()).update(any(Options.class));
    }

    @Test
    public void getAvailableToAddIncompatibleOptionsTest() throws OptionException {
        when(optionRepository.findById(anyLong())).thenReturn(getOptionsEntity());
        when(optionRepository.findAllActive()).thenReturn(getAllTestOptions());
        List<Options> test = optionsService.getAvailableToAddIncompatibleOptions(getOptionsEntity().getId());
        assertEquals(1, test.size());
    }

    @Test
    public void getAvailableToAddLinkedOptionsTest() throws OptionException {
        when(optionRepository.findById(anyLong())).thenReturn(getOptionsEntity());
        when(optionRepository.findAllActive()).thenReturn(getAllTestOptions());
        List<Options> test = optionsService.getAvailableToAddLinkedOptions(getOptionsEntity().getId());
        assertEquals(1, test.size());
    }

    @Test
    public void saveTest() throws OptionException {
        when(optionsToEntityMapper.dtoToEntity(any())).thenReturn(getOptionsEntity());
        optionsService.save(getOptionsDto());
        verify(optionRepository, only()).save(getOptionsEntity());
    }
}
