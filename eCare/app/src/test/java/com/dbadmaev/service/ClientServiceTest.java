package com.dbadmaev.service;

import com.dbadmaev.tschool.eCare.dto.client.ClientsDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.exceptions.ClientException;
import com.dbadmaev.tschool.eCare.mapper.client.ClientsMapper;
import com.dbadmaev.tschool.eCare.repository.interfaces.clients.ClientRepository;
import com.dbadmaev.tschool.eCare.service.impl.clients.ClientServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {
    @Mock
    private ClientRepository clientRepository;

    @Mock
    private ClientsMapper clientsMapper;

    @InjectMocks
    private ClientServiceImpl clientService;

    private Clients getClientsEntity() {
        return Clients.builder()
                .id(4)
                .version(1)
                .blocked(0)
                .role("USER")
                .name("Pavel")
                .secondName("Pavlov")
                .dateOfBirth(LocalDate.of(2006, 2,14))
                .passportData("4413345678")
                .address("Volzhskaya Ul., bld. 24, appt. 61")
                .email("ppavlovTest@mail.ru")
                .password("test")
                .build();
    }

    private ClientsDto getClientsDto() {
        return ClientsDto.builder()
                .id(4)
                .blocked(0)
                .name("Pavel")
                .secondName("Pavlov")
                .dateOfBirth(LocalDate.of(2006, 2,14))
                .passportData("4413345678")
                .address("Volzhskaya Ul., bld. 24, appt. 61")
                .email("ppavlovTest@mail.ru")
                .build();
    }

    @Test
    public void findAllTest() throws ClientException {
        when(clientsMapper.toDtoList(anyList())).thenReturn(Collections.singletonList(getClientsDto()));
        when(clientRepository.findAll()).thenReturn(Collections.singletonList(getClientsEntity()));
        List<ClientsDto> test = clientService.findAll();
        assertEquals(test.get(0), getClientsDto());
    }

    @Test
    public void findByIdTest() throws ClientException {
        when(clientsMapper.toDto(any())).thenReturn(getClientsDto());
        lenient().when(clientRepository.findById(anyLong())).thenReturn(getClientsEntity());
        ClientsDto test = clientService.findById(4);
        assertEquals(test, getClientsDto());
    }
}
