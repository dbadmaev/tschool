package com.dbadmaev.service;

import com.dbadmaev.tschool.eCare.dto.contract.ContractsDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.entity.Contracts;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.Tariffs;
import com.dbadmaev.tschool.eCare.entity.enums.ContractStatus;
import com.dbadmaev.tschool.eCare.entity.enums.OptionType;
import com.dbadmaev.tschool.eCare.exceptions.ContractException;
import com.dbadmaev.tschool.eCare.mapper.contract.ContractsMapper;
import com.dbadmaev.tschool.eCare.repository.interfaces.contracts.ContractRepository;
import com.dbadmaev.tschool.eCare.service.impl.contracts.ContractServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ContractServiceTest {
    @Mock
    private ContractRepository contractRepository;

    @Mock
    private ContractsMapper contractsMapper;

    @InjectMocks
    private ContractServiceImpl contractService;

    private Clients getClientsEntity() {
        return Clients.builder()
                .id(4)
                .version(1)
                .blocked(0)
                .role("USER")
                .name("Pavel")
                .secondName("Pavlov")
                .dateOfBirth(LocalDate.of(2006, 2,14))
                .passportData("4413345678")
                .address("Volzhskaya Ul., bld. 24, appt. 61")
                .email("ppavlovTest@mail.ru")
                .password("test")
                .build();
    }

    private List<Options> getIncompatibleOptions() {
        List<Options> incList = new ArrayList<>();
        incList.add(Options.builder()
                .id(2)
                .version(1)
                .optionType(OptionType.CALLS)
                .name("test")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .build());
        return incList;
    }

    private List<Options> getLinkedOptions() {
        List<Options> linkedOpts = new ArrayList<>();
        linkedOpts.add(Options.builder()
                .id(3)
                .version(1)
                .optionType(OptionType.INTERNET)
                .name("test")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .build());
        return linkedOpts;
    }

    private Options getOptionsEntity() {
        return Options.builder()
                .id(1)
                .version(1)
                .optionType(OptionType.INTERNET)
                .name("test")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .incompatibleOptionsList(getIncompatibleOptions())
                .linkedOptionsList(getLinkedOptions())
                .build();
    }

    private List<Options> getOptionsList() {
        List<Options> options = new ArrayList<>();
        options.add(getOptionsEntity());
        return options;
    }

    private Tariffs getTariffsEntity() {
        return Tariffs.builder()
                .id(2)
                .version(1)
                .status(1)
                .name("test")
                .price(BigDecimal.ZERO)
                .build();
    }

    private Contracts getContractsEntity() {
        return Contracts.builder()
                .id(6)
                .version(1)
                .status(ContractStatus.ACTIVE)
                .number("+79110000001")
                .client(getClientsEntity())
                .build();
    }

    private ContractsDto getContractsDto() {
        return ContractsDto.builder()
                .id(6)
                .client(getClientsEntity())
                .tariff(getTariffsEntity())
                .status("ACTIVE")
                .number("+79110000001")
                .build();
    }

    @Test
    public void findAllTest() throws ContractException {
        when(contractsMapper.toDtoList(anyList())).thenReturn(Collections.singletonList(getContractsDto()));
        when(contractRepository.findAll()).thenReturn(Collections.singletonList(getContractsEntity()));
        List<ContractsDto> test = contractService.findAll();
        assertEquals(test.get(0), getContractsDto());
    }

    @Test
    public void findByIdTest() throws ContractException {
        when(contractsMapper.toDto(any())).thenReturn(getContractsDto());
        when(contractRepository.findById(anyLong())).thenReturn(getContractsEntity());
        ContractsDto test = contractService.findById(6);
        assertEquals(test, getContractsDto());
    }

    @Test
    public void findContractsByClientIdTest() throws ContractException {
        when(contractsMapper.toDtoList(any())).thenReturn(Collections.singletonList(getContractsDto()));
        when(contractRepository.findByClientId(anyLong())).thenReturn(Collections.singletonList(getContractsEntity()));
        List<ContractsDto> test = contractService.findContractsByClientId(6);
        assertEquals(test.get(0), getContractsDto());
    }

    @Test
    public void numGeneratorTest() throws ContractException {
        when(contractRepository.findLastContract()).thenReturn(getContractsEntity());
        String test = contractService.numGenerator();
        assertEquals(test, "+79110000002");
    }

    @Test
    public void deleteOptionTest() throws ContractException {
        lenient().when(contractRepository.findAll()).thenReturn(Collections.singletonList(getContractsEntity()));
        contractRepository.update(getContractsEntity());
        verify(contractRepository, only()).update(any(Contracts.class));
    }

    @Test
    public void blockContractTest() throws ContractException {
        lenient().when(contractRepository.findByClientId(anyLong())).thenReturn(Collections.singletonList(getContractsEntity()));
        contractRepository.update(getContractsEntity());
        verify(contractRepository, only()).update(any(Contracts.class));
    }

    @Test
    public void unblockContractTest() throws ContractException {
        lenient().when(contractRepository.findByClientId(anyLong())).thenReturn(Collections.singletonList(getContractsEntity()));
        contractRepository.update(getContractsEntity());
        verify(contractRepository, only()).update(any(Contracts.class));
    }
}
