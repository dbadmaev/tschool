package com.dbadmaev.service;

import com.dbadmaev.tschool.eCare.dto.option.OptionsDto;
import com.dbadmaev.tschool.eCare.dto.tariff.TariffsDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.entity.Contracts;
import com.dbadmaev.tschool.eCare.entity.Options;
import com.dbadmaev.tschool.eCare.entity.Tariffs;
import com.dbadmaev.tschool.eCare.entity.enums.ContractStatus;
import com.dbadmaev.tschool.eCare.entity.enums.OptionType;
import com.dbadmaev.tschool.eCare.exceptions.TariffException;
import com.dbadmaev.tschool.eCare.mapper.tariff.TariffsMapper;
import com.dbadmaev.tschool.eCare.repository.interfaces.tariffs.TariffRepository;
import com.dbadmaev.tschool.eCare.service.impl.tariffs.TariffServiceImpl;
import com.dbadmaev.tschool.eCare.service.interfaces.options.OptionsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TariffServiceTest {

    @Mock
    private OptionsService optionsService;

    @Mock
    private TariffsMapper tariffsMapper;

    @Mock
    private TariffRepository tariffRepository;

    @InjectMocks
    private TariffServiceImpl tariffService;

    private Options getOptionsEntity() {
        return Options.builder()
                .id(1)
                .version(1)
                .active(true)
                .optionType(OptionType.INTERNET)
                .name("test")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .incompatibleOptionsList(getIncompatibleOptions())
                .linkedOptionsList(getLinkedOptions())
                .build();
    }

    private Options getOptionsToAddEntity() {
        return Options.builder()
                .id(4)
                .version(1)
                .active(true)
                .optionType(OptionType.INTERNET)
                .name("test4")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .incompatibleOptionsList(getIncompatibleOptions())
                .linkedOptionsList(getLinkedOptions())
                .build();
    }

    private List<Options> getIncompatibleOptions() {
        List<Options> incList = new ArrayList<>();
        incList.add(Options.builder()
                .id(2)
                .version(1)
                .active(true)
                .optionType(OptionType.CALLS)
                .name("testInc")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .build());
        return incList;
    }

    private List<Options> getLinkedOptions() {
        List<Options> linkedOpts = new ArrayList<>();
        linkedOpts.add(Options.builder()
                .id(3)
                .version(1)
                .active(true)
                .optionType(OptionType.INTERNET)
                .name("testLinked")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .build());
        return linkedOpts;
    }

    private List<OptionsDto> getOptionsDtoList() {
        List<OptionsDto> optionsDtoList = new ArrayList<>();
        optionsDtoList.add(OptionsDto.builder()
                .id(1)
                .optionType("INTERNET")
                .active(true)
                .name("test")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .build());
        return optionsDtoList;
    }

    private List<Options> getOptionsList() {
        List<Options> optionsList = new ArrayList<>();
        optionsList.add(Options.builder()
                .id(1)
                .version(1)
                .active(true)
                .optionType(OptionType.INTERNET)
                .name("test")
                .price(BigDecimal.ZERO)
                .connectionCost(BigDecimal.ZERO)
                .incompatibleOptionsList(getIncompatibleOptions())
                .linkedOptionsList(getLinkedOptions())
                .build());
        return optionsList;
    }

    private Clients getClientsEntity() {
        return Clients.builder()
                .id(4)
                .version(1)
                .blocked(0)
                .role("USER")
                .name("Pavel")
                .secondName("Pavlov")
                .dateOfBirth(LocalDate.of(2006, 2,14))
                .passportData("4413345678")
                .address("Volzhskaya Ul., bld. 24, appt. 61")
                .email("ppavlovTest@mail.ru")
                .password("test")
                .build();
    }

    private List<Contracts> getContractsList() {
        List<Contracts> contractsList = new ArrayList<>();
        contractsList.add(Contracts.builder()
                .id(6)
                .version(1)
                .status(ContractStatus.ACTIVE)
                .number("+79110000001")
                .client(getClientsEntity())
                .build());
        return contractsList;
    }

    private List<Tariffs> getTariffsList() {
        List<Tariffs> tariffsList = new ArrayList<>();
        tariffsList.add(getTariffEntity());
        return tariffsList;
    }

    private Tariffs getTariffEntity() {
        return Tariffs.builder()
                .id(1)
                .version(1)
                .name("test")
                .price(BigDecimal.ZERO)
                .status(1)
                .optionsSet(getOptionsList())
                .contractsList(getContractsList())
                .build();
    }

    private TariffsDto getTariffDto() {
        return TariffsDto.builder()
                .id(1)
                .name("test")
                .price(BigDecimal.ZERO)
                .status(1)
                .possibleOptionsDtoList(getOptionsDtoList())
                .build();
    }

    private List<TariffsDto> getTariffsDtoList() {
        List<TariffsDto> tariffsDtoList = new ArrayList<>();
        tariffsDtoList.add(getTariffDto());
        return tariffsDtoList;
    }

    @Test
    public void getTariffTest() throws TariffException {
        when(tariffRepository.findById(anyLong())).thenReturn(getTariffEntity());
        when(tariffsMapper.toDto(any())).thenReturn(getTariffDto());
        TariffsDto test = tariffService.getTariff(getTariffEntity().getId());
        assertEquals(getTariffDto(), test);
    }

    @Test
    public void getTariffListTest() throws TariffException {
        when(tariffRepository.findAllActive()).thenReturn(Collections.singletonList(getTariffEntity()));
        when(tariffsMapper.toDtoList(anyList())).thenReturn(getTariffsDtoList());
        List<TariffsDto> test = tariffService.getTariffsList();
        assertEquals(getTariffsDtoList(), test);
    }

    @Test
    public void deleteTariffTest() throws TariffException {
        lenient().when(tariffRepository.findById(anyLong())).thenReturn(getTariffEntity());
        tariffRepository.update(getTariffEntity());
        verify(tariffRepository, only()).update(any(Tariffs.class));
    }

    @Test
    public void checkForIncompatibleOptionsTest() {
        boolean hasIncOpts = tariffService.checkForIncompatibleOptions(getOptionsList().get(0));
        assertEquals(getIncompatibleOptions().size() > 0, hasIncOpts);
    }

    @Test
    public void checkForLinkedOptionsTest() {
        boolean hasLinkedOpts = tariffService.checkForLinkedOptions(getOptionsList().get(0));
        assertEquals(getLinkedOptions().size() > 0, hasLinkedOpts);
    }

    @Test
    public void addOptionTest() throws TariffException {
        lenient().when(tariffRepository.findById(anyLong())).thenReturn(getTariffEntity());
        lenient().when(optionsService.findById(anyLong())).thenReturn(getOptionsToAddEntity());
        tariffRepository.update(getTariffEntity());
        verify(tariffRepository, only()).update(any(Tariffs.class));
    }
}
