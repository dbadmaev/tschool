package com.dbadmaev.tschool.eCare.mapper.client;

import com.dbadmaev.tschool.eCare.dto.client.ClientsRegistrationDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.entity.Clients.ClientsBuilder;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-06-22T14:34:38+0300",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 11.0.11 (Amazon.com Inc.)"
)
@Component
public class ClientsRegistrationMapperImpl implements ClientsRegistrationMapper {

    @Override
    public ClientsRegistrationDto toDto(Clients clients) {
        if ( clients == null ) {
            return null;
        }

        ClientsRegistrationDto clientsRegistrationDto = new ClientsRegistrationDto();

        clientsRegistrationDto.setRole( clients.getRole() );
        clientsRegistrationDto.setName( clients.getName() );
        clientsRegistrationDto.setSecondName( clients.getSecondName() );
        if ( clients.getDateOfBirth() != null ) {
            clientsRegistrationDto.setDateOfBirth( DateTimeFormatter.ISO_LOCAL_DATE.format( clients.getDateOfBirth() ) );
        }
        clientsRegistrationDto.setPassportData( clients.getPassportData() );
        clientsRegistrationDto.setAddress( clients.getAddress() );
        clientsRegistrationDto.setEmail( clients.getEmail() );
        clientsRegistrationDto.setPassword( clients.getPassword() );

        return clientsRegistrationDto;
    }

    @Override
    public Clients dtoToEntity(ClientsRegistrationDto clientsRegistrationDto) {
        if ( clientsRegistrationDto == null ) {
            return null;
        }

        ClientsBuilder clients = Clients.builder();

        clients.role( clientsRegistrationDto.getRole() );
        clients.name( clientsRegistrationDto.getName() );
        clients.secondName( clientsRegistrationDto.getSecondName() );
        if ( clientsRegistrationDto.getDateOfBirth() != null ) {
            clients.dateOfBirth( LocalDate.parse( clientsRegistrationDto.getDateOfBirth() ) );
        }
        clients.passportData( clientsRegistrationDto.getPassportData() );
        clients.address( clientsRegistrationDto.getAddress() );
        clients.email( clientsRegistrationDto.getEmail() );
        clients.password( clientsRegistrationDto.getPassword() );

        return clients.build();
    }
}
