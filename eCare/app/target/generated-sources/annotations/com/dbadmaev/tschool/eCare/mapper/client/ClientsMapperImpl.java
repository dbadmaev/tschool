package com.dbadmaev.tschool.eCare.mapper.client;

import com.dbadmaev.tschool.eCare.dto.client.ClientsDto;
import com.dbadmaev.tschool.eCare.entity.Clients;
import com.dbadmaev.tschool.eCare.entity.Clients.ClientsBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-06-22T14:34:37+0300",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 11.0.11 (Amazon.com Inc.)"
)
@Component
public class ClientsMapperImpl implements ClientsMapper {

    @Override
    public ClientsDto toDto(Clients clients) {
        if ( clients == null ) {
            return null;
        }

        ClientsDto clientsDto = new ClientsDto();

        clientsDto.setId( clients.getId() );
        clientsDto.setBlocked( clients.getBlocked() );
        clientsDto.setName( clients.getName() );
        clientsDto.setSecondName( clients.getSecondName() );
        clientsDto.setDateOfBirth( clients.getDateOfBirth() );
        clientsDto.setPassportData( clients.getPassportData() );
        clientsDto.setAddress( clients.getAddress() );
        clientsDto.setEmail( clients.getEmail() );

        return clientsDto;
    }

    @Override
    public List<ClientsDto> toDtoList(List<Clients> clientsList) {
        if ( clientsList == null ) {
            return null;
        }

        List<ClientsDto> list = new ArrayList<ClientsDto>( clientsList.size() );
        for ( Clients clients : clientsList ) {
            list.add( toDto( clients ) );
        }

        return list;
    }

    @Override
    public Clients dtoToEntity(ClientsDto clientsDto) {
        if ( clientsDto == null ) {
            return null;
        }

        ClientsBuilder clients = Clients.builder();

        clients.name( clientsDto.getName() );
        clients.secondName( clientsDto.getSecondName() );
        clients.dateOfBirth( clientsDto.getDateOfBirth() );
        clients.passportData( clientsDto.getPassportData() );
        clients.address( clientsDto.getAddress() );
        clients.email( clientsDto.getEmail() );
        clients.id( clientsDto.getId() );
        clients.blocked( clientsDto.getBlocked() );

        return clients.build();
    }
}
