package com.dbadmaev.tschool.eCare.mapper.tariff;

import com.dbadmaev.tschool.eCare.dto.tariff.TariffsDto;
import com.dbadmaev.tschool.eCare.entity.Tariffs;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-06-22T14:34:38+0300",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 11.0.11 (Amazon.com Inc.)"
)
@Component
public class TariffsMapperImpl implements TariffsMapper {

    @Override
    public TariffsDto toDto(Tariffs tariffs) {
        if ( tariffs == null ) {
            return null;
        }

        TariffsDto tariffsDto = new TariffsDto();

        tariffsDto.setId( tariffs.getId() );
        tariffsDto.setStatus( tariffs.getStatus() );
        tariffsDto.setName( tariffs.getName() );
        tariffsDto.setPrice( tariffs.getPrice() );

        return tariffsDto;
    }

    @Override
    public List<TariffsDto> toDtoList(List<Tariffs> tariffsList) {
        if ( tariffsList == null ) {
            return null;
        }

        List<TariffsDto> list = new ArrayList<TariffsDto>( tariffsList.size() );
        for ( Tariffs tariffs : tariffsList ) {
            list.add( toDto( tariffs ) );
        }

        return list;
    }
}
