package com.dbadmaev.tschool.eCare.mapper.admin;

import com.dbadmaev.tschool.eCare.dto.admin.AdminsDto;
import com.dbadmaev.tschool.eCare.entity.Admins;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-06-22T14:34:38+0300",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 11.0.11 (Amazon.com Inc.)"
)
@Component
public class AdminsMapperImpl implements AdminsMapper {

    @Override
    public AdminsDto toDto(Admins admins) {
        if ( admins == null ) {
            return null;
        }

        AdminsDto adminsDto = new AdminsDto();

        adminsDto.setId( admins.getId() );
        adminsDto.setName( admins.getName() );
        adminsDto.setPassword( admins.getPassword() );

        return adminsDto;
    }

    @Override
    public List<AdminsDto> toDtoList(List<Admins> adminsList) {
        if ( adminsList == null ) {
            return null;
        }

        List<AdminsDto> list = new ArrayList<AdminsDto>( adminsList.size() );
        for ( Admins admins : adminsList ) {
            list.add( toDto( admins ) );
        }

        return list;
    }
}
