create table tariffs_options
(
	id bigint auto_increment
		primary key,
	version bigint default 0 not null,
	tariff_id bigint null,
	option_id bigint null,
	constraint tariffs_options_ibfk_1
		foreign key (tariff_id) references tariffs (id),
	constraint tariffs_options_ibfk_2
		foreign key (option_id) references options (id)
);

create index option_id
	on tariffs_options (option_id);

create index tariff_id
	on tariffs_options (tariff_id);

