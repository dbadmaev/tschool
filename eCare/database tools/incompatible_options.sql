create table incompatible_options
(
	id bigint auto_increment
		primary key,
	version bigint default 0 not null,
	option_one_id bigint null,
	option_two_id bigint null,
	constraint incompatible_options_options_id_fk
		foreign key (option_one_id) references options (id),
	constraint incompatible_options_options_id_fk_2
		foreign key (option_two_id) references options (id)
);

