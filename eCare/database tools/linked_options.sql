create table linked_options
(
	id bigint auto_increment
		primary key,
	version bigint default 0 not null,
	option_id bigint null,
	linked_option bigint null,
	constraint linkedOptions_options_id_fk
		foreign key (option_id) references options (id),
	constraint linkedOptions_options_id_fk_2
		foreign key (linked_option) references options (id)
);

