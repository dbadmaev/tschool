create table options
(
	id bigint auto_increment
		primary key,
	version bigint default 0 not null,
	type varchar(50) not null,
	name varchar(50) not null,
	price decimal(15,2) not null,
	connection_cost decimal(15,2) not null
);

