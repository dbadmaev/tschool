create table contracts
(
	id bigint auto_increment
		primary key,
	version bigint default 0 not null,
	status varchar(50) not null,
	number varchar(20) not null,
	client_id bigint null,
	tariff_id bigint null,
	constraint contracts_ibfk_1
		foreign key (client_id) references clients (id),
	constraint contracts_ibfk_2
		foreign key (tariff_id) references tariffs (id)
);

create index client_id
	on contracts (client_id);

create index tariff_id
	on contracts (tariff_id);

