create table contracts_options
(
	id bigint auto_increment
		primary key,
	version bigint default 0 not null,
	contract_id bigint null,
	option_id bigint null,
	constraint contracts_options_contracts_id_fk
		foreign key (contract_id) references contracts (id),
	constraint contracts_options_options_id_fk
		foreign key (option_id) references options (id)
);

