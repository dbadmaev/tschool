create table tariffs
(
	id bigint auto_increment
		primary key,
	version bigint default 0 not null,
	status int null,
	name varchar(50) not null,
	price decimal(15,2) not null
);

