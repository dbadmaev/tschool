create table admins
(
	id bigint auto_increment
		primary key,
	version bigint default 0 not null,
	role varchar(5) default 'ADMIN' not null,
	name varchar(50) not null,
	password varchar(100) not null
);

