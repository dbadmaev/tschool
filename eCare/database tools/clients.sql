create table clients
(
	id bigint auto_increment
		primary key,
	version bigint default 0 not null,
	role varchar(4) default 'USER' not null,
	blocked int default 0 null,
	name varchar(50) not null,
	second_name varchar(50) not null,
	date_of_birth date not null,
	passport_data varchar(20) not null,
	address varchar(100) not null,
	email varchar(70) not null,
	password varchar(100) not null
);

